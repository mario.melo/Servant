/*======================================================
              *** DOCUMENT READY ***
======================================================*/

$(document).ready(function() {
  /* ------------- niceScroll -------------- */
  $("html").niceScroll();

  /* ------------- Modal -------------- */
  $('#modal').modal('show')

  /* ------------- mapHome popovers -------------- */
  $('#RS, #SC, #PR, #SP, #RJ, #MG, #ES, #BA, #SE, #AL, #PE, #PB, #RN, #CE, #PI, #MA, #TO, #DF, #GO, #MS, #MT, #PA, #AP, #RR, #AM, #AC, #RO').popover();

  /* ------------- modalLoginCadastro -------------- */
  /*
  $("#btnLogin").click(function(){
      $('#modalCadastro').css("display","none !important");
    });
    */

});

/*======================================================
                *** WINDOW LOAD ***
======================================================*/

/* ------------- scrollerCustom -------------- */
$(window).load(function() {
  $("#servicos_categorias .panel-body").mCustomScrollbar();
});

/*======================================================
              *** FUNCTIONS ***
======================================================*/
/* ------------- WOW animate -------------- */

 new WOW().init();

/* ------------- scrollerCalc -------------- */
$(window).scroll(function() {
  var y = $(window).scrollTop();
  if (y > 230) {
    if ($(".conteudo").height() > 1000) {
      $("body").addClass("topo")
    }
  } else {
    $("body").removeClass("topo")
  }
});

/* ------------- mapa Servicos -------------- */
$("#abrirMapa").click(function() {
  $("#servicosMapa").addClass("mapaAberto");
});
$("#fecharMapa").click(function() {
  $("#servicosMapa").removeClass("mapaAberto");
});

/* - Range - */
$("#mapRange").slider({
	tooltip: 'always'
});

/* ------------- botaoVerTelefone -------------- */
$(".btnAcoes .verTelefone").click(function() {
  $(this).addClass("wow flipInX animated");
});

/* ------------- menuLateral -------------- */
function toggleChevron(e) {
  $(e.target)
    .prev('.panel-heading')
    .find("i.indicator")
    .toggleClass('icon-plus icon-minus btnLaranja btnAzul');
}
$('#servicos_categorias').on('hidden.bs.collapse', toggleChevron);
$('#servicos_categorias').on('shown.bs.collapse', toggleChevron);

/* ------------- owlCarousel -------------- */
$(".perfilPrestador .servicoSlider").owlCarousel({

      autoPlay: 3000, //Set AutoPlay to 3 seconds
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true

  });
