var appConfig = {
		servidor: '/servant',
		versao_app: '1.0.0.SNAPSHOT',

		ambiente: {
			isNotMinify : 'true'
		},
		baseUrl :  '/backend' ,
		defaultRoute : 'mapa',
		login: {
			url: '/backend'  + '/oauth/token',//'login.json',
			url_usuario: '/backend'  +  '/api/private/login/dadosUsuarioLogado',// 'usuario.json',
			sucesso: 'manterServidor',
			clientId : 'servant',
			clientSecret: 'servant'
		},
		logout: {
			url: '/backend/api/private/login/logout'//logout.json
		}
};

appConfig.baseUrlPrivate =  appConfig.baseUrl +  '/api/private' ;
appConfig.baseUrlPublic = appConfig.baseUrl +  '/api/public' ;

var libDir =  'lib/';
var apiDir = appConfig.servidor + '/vendor/angular/api/' + appConfig.versao_arquitetura + '/';
var componentesDir = libDir + 'componentes/';
var bibliotecasDir =  libDir + 'vendor/';
var modulosDir = 'modules/';
var layoutDir = 'layout/';

requirejs.config({
	baseUrl:'app',
	paths: {
		'app'		: [ 'app'],
		'config'	: [ 'config'],
		'init'		: [libDir + 'init']
	},
	shim: {
		'app': {deps: ['init'], exports: 'app'},
		'config': {deps: ['init'], exports: 'config' }
	}, deps: ['app']
});

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2-lat1);  // deg2rad below
	var dLon = deg2rad(lon2-lon1);
	var a =
		Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
		Math.sin(dLon/2) * Math.sin(dLon/2)
		;
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c; // Distance in km
	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI/180)
}

var _createRoute = function(rotas){

	var isMinify = appConfig.ambiente.isNotMinify == 'true';

	if(!isMinify) {
		sufixo = '.min';
		rotasMin = [];
		angular.forEach(rotas, function(value, key) {
			rotasMin.push(value + sufixo);
		});
		return rotasMin;
	}else
		return rotas;
};