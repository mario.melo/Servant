define(
		[
			'angularAMD',
			'uiRouter',
			'config',
			'wow',
			'ngMask',
			'angular',
			'iosRoute',
			'nicescroll',
			'infiniteScroll',
			'angular-translate',
			'angular-trans-static',
			'schema-form-bootstrap',
			'angular-trans-part',
			'angular-block-ui',
			'notification',
			'schema-form',
			'bootstrap',
			'sanitize',
			'ng-quill',
			'hellojs',
			'notify',
			'ng-map',
			'layout',
			'owlcarousel',
			'oauthServant',
			'securityServant',
			'bootstrap-slider',	
			'angular-cookies',
			'angularTouch',
			'fileUpload',
			'ngstorage',
			'lodash',
			],
			function(angularAMD, uiRouter, config) {


			'use strict';
			var app = angular.module('webapp', ['restangular',
				'pascalprecht.translate',
				'servantSeguranca',
				'infinite-scroll',
				'oauthServant',
				'ngFileUpload',
				'schemaForm',
				'ngSanitize',
				'ngStorage',
				'ui.router',
				'iosRoute',
				'ngTouch',
				'blockUI',
				'ngQuill',
				'config',
				'ngMask',
				'ngMap'
				])

				app.run(['restFactory',
					function(restFactory){

				}]);

			app.factory('sessionInjector', ['$cookieStore', function($cookieStore) {
				var sessionInjector = {
						request: function(config) {

							if( $cookieStore.get('servantToken') && config.url.substr(0, appConfig.baseUrlPrivate.length) === appConfig.baseUrlPrivate){
								var token = 'Bearer ' + $cookieStore.get('servantToken').replace(/[\\"]/g, '');
								config.headers['Authorization'] = token;
							}
							return config;
						},
						responseError: function(response){
							return response;
						}
				};
				return sessionInjector;
			}]);

			app.config(['$httpProvider','$translateProvider', '$injector', 'blockUIConfig', function($httpProvider, $translateProvider, $injector, blockUIConfig){

				blockUIConfig.autoBlock = true;

				blockUIConfig.template = '<img id="blocker-img" src="app/layout/images/preloader.gif"/> <div id="blocker"></div>';

				$httpProvider.interceptors.push('sessionInjector');

				$translateProvider.useStaticFilesLoader({
					files: [{
						prefix: 'app/I18n/',
						suffix: '.json'
					}]
				});

				$translateProvider.preferredLanguage('pt_BR');

			}]);
			return angularAMD.bootstrap(app);
		}
);