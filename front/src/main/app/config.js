requirejs.config({
	baseUrl:'app',

	paths:{
		'loginRoute'  	: [modulosDir + 'login/routes/loginRoute'],
		'mapaRoute'  	: [modulosDir + 'mapa/routes/mapaRoute'],
		'homeRoute'  	: [modulosDir + 'home/routes/homeRoute'],
		'userPageRoute' : [modulosDir + 'userPage/routes/userPageRoute'],
		'consultaRoute' : [modulosDir + 'consulta/routes/consultaRoute']
	}
});

(function(){
	var dependencies = [
	                    'angular',
	                    'loginRoute',
	                    'mapaRoute',
	                    'homeRoute',
	                    'userPageRoute',
	                    'consultaRoute',
	                    'tv4'
	                    ]

	define(dependencies, function(angular, login, mapa, home, userPage, consultaRoute, tv4){

		window.tv4 = tv4;

		var app = angular.module('config',[]);

		app.factory('restFactory',['$rootScope', 'iosRouteService','$cookies','$http', '$state',
		                           function($rootScope, iosRouteService, $cookies, $http, $state){
			iosRouteService.create(login);
			iosRouteService.create(mapa);
			iosRouteService.create(home);
			iosRouteService.create(userPage);
			iosRouteService.create(consultaRoute);


			$rootScope.$on('$stateChangeSuccess',
					  function(event, toState, toParams, fromState, fromParams) {
						$rootScope.currentState = $state.current.module;

						if($rootScope.currentState ==  'mapa')
						$rootScope.__class = 'selecionarRegiao'
						else
							$rootScope.__class = '';
					  }
					);


			var resourceUrl = {
					google 		:  appConfig.baseUrl + '/api/public/google',
					facebook	:  appConfig.baseUrl + '/api/public/facebook',
					comum  		:  appConfig.baseUrl + '/api/comum',
					login  		:  appConfig.baseUrl + '/api/private/login',
					userPage    :  appConfig.baseUrl +  '/api/private/usuario',
					anuncio     :  appConfig.baseUrl +  '/api/private/anuncios',
					consulta	:  appConfig.baseUrl +  '/api/public/anuncio'
			};

			return {
				resourceUrl: resourceUrl
			};

			return app;
		}]);
	});
}());

