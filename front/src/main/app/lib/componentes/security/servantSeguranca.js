! function() {
	'use strict';
	var servantSeguranca = angular.module('servantSeguranca', ['ngCookies']);

	servantSeguranca.run(['$rootScope', 'servantSegurancaService', '$state', 'servantAutenticacaoService',
		function($rootScope, servantSegurancaService, $state, servantAutenticacaoService) {

		$rootScope.$on('$stateChangeError', function(ev, to, toParams, from, fromParams, error) {
			if (error.status === 401 && servantSegurancaService.isUsuarioAutenticado() && servantSegurancaService.getToken()) {
				servantSegurancaService.setUsuarioAutenticado(false);
				$state.go('login');
			}
		});
	}
	]);

	servantSeguranca.factory('httpInterceptor', ['$cookieStore', function($cookieStore){
		return {
			request: function(config) {
				if( $cookieStore.get('servantToken') && config.url.substr(0, appConfig.baseUrlPrivate.length) === appConfig.baseUrlPrivate){
					var token = 'Bearer ' + $cookieStore.get('servantToken');
					config.headers['Authorization'] = token;
				}
				return config;
			},

			requestError: function(config) {
				return config;
			},

			response: function(res) {
				return res;
			},

			responseError: function(res) {
				return res;
			}
		}
	}]);


	servantSeguranca.config(['$httpProvider', function($httpProvider){
		$httpProvider.interceptors.push('httpInterceptor');
	}]);

	return servantSeguranca;

}();

! function() {
	'use strict';

	var servantSeguranca = angular.module('servantSeguranca');

	servantSeguranca.factory('servantSegurancaService', ['$cookieStore', '$q', '$rootScope', '$injector', function($cookieStore, $q, $rootScope, $injector) {

		var _usuarioAutenticado, _token;

		var setUsuario = function(usuarioAutenticado) {
			$rootScope.usuarioAutenticado = usuarioAutenticado;
			_usuarioAutenticado = usuarioAutenticado;
			$rootScope.usuarioAutenticado = usuarioAutenticado;
		};

		var getUsuario = function() {
			return _usuarioAutenticado;
			// return $rootScope.usuarioAutenticado;
		};

		var setUsuarioAutenticado = function(value) {

			if (true === value) {
				$cookieStore.put('isUsuarioAutenticado', value);
				contador();
			} else {
				destruirSessao();
			}
		};


		var setTempoLimite = function(value) {
			try {
				var tempoLimite = new Date(getTempoInicial() + value * 60000);
				$cookieStore.put('tempoLimite', tempoLimite.getTime());
			} catch (e) {
				$rootScope.$servantNotify.error(e);
			}
		};

		var getTempoLimite = function() {
			return $cookieStore.get('tempoLimite');
		};

		var isUsuarioAutenticado = function() {
			return $cookieStore.get('isUsuarioAutenticado');
		};

		var possuiAcesso = function(rolesPermitidas, usuario) {
			var deferred = $q.defer();
			var usuario = (!usuario) ? getUsuario() : usuario;

			if (typeof usuario != 'undefined') {

				var possui = false;
				if (rolesPermitidas) {
					angular.forEach(usuario.roles, function(val) {
						if (rolesPermitidas.indexOf(val) != -1) {
							possui = true;
						}
					});

					if (rolesPermitidas.indexOf('*') != -1) {
						possui = true;
					}
				}

				if (possui) {
					deferred.resolve(this);
				} else {
					if (rolesPermitidas)
						deferred.reject('Usuário sem permissão de acesso');
					else
						deferred.reject('A funcionalidade requerida só pode ser acessada publicamente.');
				}

				return deferred.promise;
			}

			return deferred.promise;
		};


		var setTempoInicial = function(value) {
			$cookieStore.put('tempoInicial', value);
		};

		var getTempoInicial = function() {
			return $cookieStore.get('tempoInicial');
		};

		var destruirSessao = function() {
			$cookieStore.remove('tempoInicial');
			$cookieStore.remove('tempoLimite');
			$cookieStore.remove('isUsuarioAutenticado');
			$cookieStore.remove('servantToken');
			$cookieStore.remove('servantRefreshToken');
			delete $rootScope.usuarioAutenticado;
			delete $rootScope.isUsuarioAutenticado;
			_usuarioAutenticado = null;
			sessionStorage.clear();
			//$injector.get('Idle').unwatch();
		};


		var contador = function() {

			if (isUsuarioAutenticado()) {
				var date = new Date();
				var tempoAtual = date.getTime();
				var totalTimeOn = (getTempoLimite()) ? (tempoAtual - getTempoLimite()) / 1000 : 0;

				if (totalTimeOn > 0) {
					setUsuarioAutenticado(false);
				} else {
					setTempoInicial(date.getTime());
					setTempoLimite(appConfig.login.limite);
				}
				return getTempoLimite();
			}
		};

		var setToken = function(token) {
			if (typeof token != 'undefined') {
				$cookieStore.put('servantToken', token);
				setUsuarioAutenticado(true);
			}
		};

		var getToken = function() {
			return $cookieStore.get('servantToken');
		};

		var setRefreshToken = function(refreshToken) {
			$cookieStore.put('servantRefreshToken', refreshToken);
		}

		var getRefreshToken = function() {
			return $cookieStore.get('servantRefreshToken');
		};

		return {
			contador: contador,
			possuiAcesso: possuiAcesso,
			isUsuarioAutenticado: isUsuarioAutenticado,
			setUsuarioAutenticado: setUsuarioAutenticado,
			setUsuario: setUsuario,
			setToken: setToken,
			getToken: getToken,
			setRefreshToken: setRefreshToken,
			getRefreshToken: getRefreshToken,
			getUsuario: getUsuario
		}

	}]);

	return servantSeguranca;

}();
! function() {
	'use strict';

	var servantSeguranca = angular.module('servantSeguranca');

	servantSeguranca.factory('servantAutenticacaoService', ['$rootScope', '$q', 'servantSegurancaService', '$http',
		function($rootScope, $q, servantSegurancaService, $http) {

		var getUserData = function(deferred, token) {
			if (!servantSegurancaService.getUsuario()) {
				$http.get(
						appConfig.login.url_usuario, {
							token: token
						}
				).then(function(response) {
					servantSegurancaService.setUsuario(response.data.resultado.usuario);
					//$rootScope.$broadcast('USER_LOGGED_IN', response.data.resultado.usuario);
					deferred.resolve(servantSegurancaService.getUsuario());
				}, function(reason) {
					deferred.reject(reason);
				});
			} else {
				deferred.resolve(servantSegurancaService.getUsuario());
			}
		}

		var recuperarDadosUsuario = function() {
			var deferred = $q.defer();
			var token = servantSegurancaService.getToken();
			if (servantSegurancaService.isUsuarioAutenticado()) {
				$http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
				getUserData(deferred, token);
			} else {
				deferred.reject('Não foi possível recuperar os dados do usuário.');
			}

			return deferred.promise;

		};

		var autenticar = function(username, password) {
			var deferred = $q.defer();

			var credentials = {
					'username' : username,
					'password' : password,
					'grant_type': 'password',
					'client_secret': appConfig.login.clientSecret,
					'client_id' :  appConfig.login.clientId
			}

			$http({
				method: 'POST',
				url: appConfig.login.url,
				params: credentials
			}).then(response => {
				if(response.status && response.status == 500){
					deferred.reject(response);
				}else{

					var token = response.data.access_token;
					servantSegurancaService.setToken(token);
					servantSegurancaService.setRefreshToken(response.data.refresh_token);
					$http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
					deferred.resolve();
					//getUserData(deferred, token);
				}

			}, reason => {
				deferred.reject(reason);
			});

			return deferred.promise;
		};

		var refreshToken = function(credentials) {
			var deferred = $q.defer();

			$http({
				method: 'POST',
				url: appConfig.login.url,
				params: credentials
			}).then(function(response) {
				var token = response.data.access_token
				servantSegurancaService.setToken(token);
				servantSegurancaService.setRefreshToken(response.data.refresh_token);
				$http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
			}, function(reason) {
				deferred.reject(reason);
			});

			return deferred.promise;
		};

		var logout = function() {
			try {
				var deferred = $q.defer();

				$http({
					url: appConfig.logout.url,
					method: 'POST',
				}).success(function(data, status, headers, config) {
					servantSegurancaService.setUsuarioAutenticado(false);
					servantSegurancaService.setUsuario(false);
					delete $http.defaults.headers.common.Authorization;
					deferred.resolve(servantSegurancaService);
				});

				return deferred.promise;
			} catch (e) {
				$rootScope.$servantAlert.error(e);
			}
		};

		return {
			autenticar: autenticar,
			refreshToken: refreshToken,
			logout: logout,
			recuperarDadosUsuario: recuperarDadosUsuario
		}

	}
	]);

	return servantSeguranca;

}();
! function() {

	'use strict';

	var servantSeguranca = angular.module('servantSeguranca');

	servantSeguranca.directive('servantContador', ['$compile', function($compile) {

		return {
			restrict: 'E',
			link: function(scope, element, attrs, ctrl) {
				try {
					scope.$watch(attrs.limite, function(content) {
						var template = angular.element('<timer end-time="limite">{{minutes}}:{{seconds}}</timer>');
						element.html(template);
						$compile(element.contents())(scope);
					});

				} catch (e) {
					scope.$servantNotify.error(e);
				}
			}
		};
	}]);

	return servantSeguranca;

}();


! function() {

	'use strict';

	var servantSeguranca = angular.module('servantSeguranca');

	servantSeguranca.directive('servantSeguranca', ['servantSegurancaService', 'servantAutenticacaoService', '$parse', '$animate', '$rootScope',
		function(servantSegurancaService, servantAutenticacaoService, $parse, $animate, $rootScope) {

		return {
			restrict: 'EA',
			transclude: 'element',
			priority: 600,
			link: function(scope, element, attrs, ctrl, $transclude) {

				var rolesPermissaoArray, block, childScope;

				/*
				 * Recuperando as roles do objeto.
				 */

				if (attrs.roles.match(/,/g)) {
					var trimmed = attrs.roles.replace(/\s+/g, '');
					rolesPermissaoArray = trimmed.split(",");
				} else if (attrs.roles.match(/\*/g)) {
					rolesPermissaoArray = attrs.roles;
				} else {
					var roles = $parse(attrs.roles)(scope);
					if (typeof roles == 'undefined') {
						rolesPermissaoArray = attrs.roles.match(/\./g) ? null : attrs.roles;
					} else {
						rolesPermissaoArray = roles;
					}
				}

				/*
				 * Function para recuperação de elementos clonados
				 */
				function getBlockElements(nodes) {
					var startNode = nodes[0],
					endNode = nodes[nodes.length - 1];
					if (startNode === endNode) {
						return jQuery(startNode);
					}

					var element = startNode;
					var elements = [element];

					do {
						element = element.nextSibling;
						if (!element) break;
						elements.push(element);
					} while (element !== endNode);

					return jQuery(elements);
				}

				/*
				 * Clone de elementos que não serão exibidos. Remoção/reinserção
				 * dos elementos
				 */
				function transcluder(value) {
					if (value) {
						if (!childScope) {
							childScope = scope.$new();
							$transclude(scope, function(clone) {
								clone[clone.length++] = document.createComment(' end servantSeguranca: ' + attrs.servantSeguranca + ' ');
								block = {
										clone: clone
								};
								$animate.enter(clone, element.parent(), element);
							});
						}
					} else {
						if (childScope) {
							childScope.$destroy();
							childScope = null;
						}

						if (block) {
							$animate.leave(getBlockElements(block.clone));
							block = null;
						}
					}
				};


				scope.$on('USER_LOGGED_IN', function(event, usuario) {
					servantSegurancaService.possuiAcesso(rolesPermissaoArray, usuario).then(function(result) {
						transcluder(true);
					}, function(reason) {
						transcluder(false);
					});
				})
				/*
				 * Watch sobre usuario logado. Aqui informamos se há ou não
				 * acesso ao elemento
				 */
				scope.$watch(function() {
					return servantSegurancaService.isUsuarioAutenticado();
				}, function(val) {
					if (val == true) {
						servantSegurancaService.possuiAcesso(rolesPermissaoArray, servantSegurancaService.getUsuario()).then(function(result) {
							transcluder(true);
						}, function(reason) {
							transcluder(false);
						});
					} else {
						(rolesPermissaoArray) ? transcluder(false): transcluder(true);
					}
				});
			}
		};
	}
	]);

	return servantSeguranca;

}();


! function() {

	'use strict';

	var servantSeguranca = angular.module('servantSeguranca');

	servantSeguranca.directive('servantUsuarioInfo', ['$compile', function($compile) {
		return {
			restrict: 'E',
			link: function(scope, element, attrs, ctrl) {
				try {
					scope.$watch(attrs.usuarioAutenticado, function(usuario) {
						if (typeof usuario != 'undefined' && typeof usuario.perfil != 'undefined') {

							var template = angular.element('\n\
									<a ng-click="editUsuario()" role="button" class="configuracao">\n\
									<span class="icone icone-cog"></span>\n\
									<span class="usuarioLogado">' + usuario.nome + '</span>\n\
									</a>, \n\
									<span class="perfilUsuario">' + usuario.perfil.nome + '</span>');
							element.html(template);
							$compile(element.contents())(scope);
						}
					}, true);
				} catch (e) {
					scope.$servantNotify.error(e);
				}
			}
		};
	}]);

	return servantSeguranca;

}();
