!function() {
	'use strict';
	try {
		var modulo = angular.module('oauthServant', []);

		modulo.run(['$rootScope','$http', '$injector', function($rootScope, $http, $injector) {

			$rootScope.getPath = function(path){
				return "/" + appConfig.baseUrl + "/api/" + path;
			};

			$rootScope.getUsuarioLogado = function(){

				var servantSegurancaService = $injector.get('servantSegurancaService');

				var token = servantSegurancaService.getToken();

				if(token){
					$http({
						url: appConfig.login.url_usuario,
						method: "GET",
						headers: {'Authorization' : 'Bearer ' + token}
					}).then(function(sucesso){
						$rootScope.usuarioLogado = sucesso.data.resultado.usuario;
						sessionStorage.usuarioAutenticado = JSON.stringify($rootScope.usuarioLogado);
					},function(erro){
						$rootScope.usuarioLogado = null;
						sessionStorage.usuarioAutenticado = null;
					});
				}
			};
			$rootScope.getUsuarioLogado();
		}]);

		return modulo;
	}catch(e) {
		$log.error(e);
	}
}();

!function() {
	'use strict';

	var modulo = angular.module('oauthServant');

	modulo.controller("oauthServantController", ['$rootScope', '$scope', '$window', '$q', '$interval','$http', '$injector','$state', '$notificationService', '$location',
		function($rootScope, $scope, $window, $q, $interval, $http, $injector, $state, $notificationService, $location){
		
		
		$rootScope.redirecionarCasoLogado = function(rota){
			if(!$rootScope.usuarioLogado){
				$('#modalLogin').modal('toggle');
			}else{
				$location.path(rota)
			}
		}
		
		$scope.signin = function(){
			var restService = $injector.get('restFactory');
			return {
				google: function(){
					socialSignin(restService.google.one("signin").getRestangularUrl(),
							restService.google.one("callback").getRestangularUrl());
				},
				facebook: function(){
					socialSignin(restService.facebook.one("signin").getRestangularUrl(),
							restService.facebook.one("callback").getRestangularUrl());
				},
				servant: function(){
					var servantAutenticacaoService = $injector.get('servantAutenticacaoService');
					servantAutenticacaoService.autenticar($scope.username, $scope.password).then(function(sucesso){
						$rootScope.getUsuarioLogado();
						fecharModal();

					},function(erro){
						$notificationService.$notify.error({title: 'Erro!', message: erro.data.error_description});
					});
				}
			};
		};

		$scope.logout = function(){
			var servantAutenticacaoService = $injector.get('servantAutenticacaoService')
			var $state = $injector.get('$state')
			servantAutenticacaoService.logout().then(function(){
				$rootScope.usuarioLogado = null;
				sessionStorage.usuarioAutenticado = null
				$state.go(appConfig.defaultRoute);
			});
		};

		$scope.oahthSocial = function(params, callbackUri){

			$http({
				url: callbackUri,
				method: "GET",
				params: params
			}).then(function(response){
				setUser(response.data.resultado, response.headers().token);
				$rootScope.usuarioLogado = response.data.resultado.usuario;
				fecharModal();
			}, function(erro){
				console.log(erro);
			});
		}

		function setUser(user, token){
			var servantSegurancaService = $injector.get('servantSegurancaService');
			servantSegurancaService.setUsuario(user);
			servantSegurancaService.setToken(token);
		}

		var socialSignin = function(uri, callbackUri){
			var q = $q.defer();

			//TODO: transformar o tamanho em constantes.. cada rede tem tamanho proprio
			//Exe:. https://blog.gisspan.com/2016/05/using-angular-promise-to-wrap-up-oauth.html https://searchcode.com/codesearch/view/84959059/
			var popup = $window.open(uri, '', "top=100,left=100,width=500,height=500");

			var popupChecker = $interval(function(){

				if (window.codeServantUser != undefined){
					q.resolve(window.codeServantUser);
					popup.close();
					$interval.cancel(popupChecker);
					$scope.oahthSocial(window.codeServantUser, callbackUri);
					window.codeServantUser = undefined;
				}else if (popup.closed){
					$interval.cancel(popupChecker);
					q.reject();
				}
			}, 1000)

			return q.promise;
		};
	}]);

	return modulo;
}();