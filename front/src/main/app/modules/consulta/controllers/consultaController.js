define([ 
        'app'

        ], function(app) {


	app.controller('consultaController', [ '$scope' ,'$rootScope', 'restFactory','NgMap', '$timeout', 'consultaService','$state', "$q", '$localStorage', '$location',
	                                       function($scope, $rootScope, restFactory, NgMap, $timeout, consultaService, $state, $q, $localStorage, $location){

		$scope.consulta = {
				result:[],
				geoResult:[],
				geoOrcamento: null
		};
		
		$scope.vm = {
				uf: $state.params.uf
		};
		
		$scope.vm.trocarUf = function(){
			delete $localStorage.ufSelecionada;
			$location.path('/mapa');
		};

		var timer = null;

		$scope.util = {
				abrirMapa : false,
				radiusMap: 5000
		};
		
		$scope.carregarOcupacoes = function(ocupacao){
			
			if(ocupacao && ocupacao.ocupacoes){
				return;
			}
			
			let id = ocupacao ? ocupacao.id : null;
			
			consultaService.consultaOcupacoes(id).then(result => {
				if(ocupacao){
					ocupacao.ocupacoes = result.data.resultado;
				} else {
					$scope.vm.ocupacoes = result.data.resultado;
				}
			});
		}
		
		$scope.radiusChanged = function(event) {
			if(this.radius < 1000)
				this.setRadius(1000);
			else if(this.radius  > 10000)
				this.setRadius(10000);
			
			if($scope.util.radiusMap != this.radius)
				$scope.util.radiusMap = _.round(this.radius, -2);
			
			$scope.consulta.geoOrcamento = {
				latitude: this.center.lat(),
				longitude: this.center.lng(),
				radius: $scope.util.radiusMap
			};
		}
		
		$scope.$watch('util.radiusMap', function(value, old){
			if(value != old)
				if($scope.util.radiusMap < 1000)
					$scope.util.radiusMap = 1000;
				else if($scope.util.radiusMap  > 10000)
					$scope.util.radiusMap = 10000
		});

		$scope.spatialSearch = function(event){
			if( $scope.consulta.result.termo){
				$scope.util.abrirMapa = true;
				carregarMap().then(function(){
					if (timer){
						$timeout.cancel(timer);
					}
					timer = $timeout(function(){
						timer = null;
						var bounds = $scope.map.getBounds().toJSON();
						params = {
								termo: $scope.consulta.result.termo,
								radius: getDistanceFromLatLonInKm(bounds.east, bounds.north, bounds.west, bounds.south),
								latitude: $scope.map.center.lat(),
								longitude: $scope.map.center.lng()
						};

						consultaService.spatialSearch(params).then(function(result){
							$scope.consulta.geoResult = result.data.resultado;
						});
					},400);
				});
			}
		};

		$scope.search = function(params){
			params.uf = $state.params.uf;
			consultaService.search(params).then(function(result){
				$scope.consulta.result = result.data.resultado;
				$scope.consulta.result.termo = params.termo;
			});
		};
		$scope.search({page:1});

		$scope.carregarPosicaoAtual = function(){
			var options = {
					enableHighAccuracy: true
			};

			navigator.geolocation.getCurrentPosition(function(pos) {
				$scope.$apply(function(){
					$scope.position = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
					//$scope.selecionarLocal($scope.position);

					//$scope.view.markers.push({lat:pos.coords.latitude, lng: pos.coords.longitude});
				});
			}, 
			function(error) {
				console.log(error);
			}, options);
		};

		var carregarMap = function(){

			var deferred = $q.defer();

			if($scope.map){
				deferred.resolve();
			}else {

				NgMap.getMap().then(function(map) {
					$scope.map = map;
					$scope.carregarPosicaoAtual();
					deferred.resolve();
				}, function(erro){
					deferred.reject();
				});
			}

			return deferred.promise
		};

	}]);

	app.directive('servantSlider',['$timeout', function($timeout){

		function link($scope, $element, $attributes){

			$scope.ngModel = $scope.max / 2;
			$timeout(function(){
				$(".servant-slider").slider({
					tooltip: 'always'
				});
			});
		}

		return {
			restrict: 'E',
			scope: {
				ngModel: '=',
				id: '=',
				max:'=',
				min:'='

			},
			link:link,
			template: '<input id="{{id}}" data-slider-id="rangeSlider" type="text" data-slider-min="{{min}}" data-slider-max="{{max}}" data-slider-step="100" data-slider-value="{{max/2}}" ng-model="ngModel" class="servant-slider" />'
		}
	}

	]);

	return app;
});