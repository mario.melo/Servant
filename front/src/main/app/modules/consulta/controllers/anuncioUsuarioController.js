define([ 
	'app',
	'owlcarousel'

	], function(app) {

	app.controller('anuncioUsuarioController', [ '$scope' ,'$rootScope', 'restFactory','NgMap', '$timeout', 'consultaService', '$state', 'anuncioService',
		function($scope, $rootScope, restFactory, NgMap, $timeout, consultaService, $state, anuncioService ){
		
		consultaService.visualizar($state.params.id).then();
		
		consultaService.getAnuncio($state.params.id).then(function(result){
			$scope.anuncio = result.data.resultado;
			$timeout(function(){
				carousel();
			});
		});
		
		$scope.solicitarOrcamento = function(solicitacao){
			anuncioService.solicitarOrcamento({'pedido': solicitacao, 'anuncioId' : $state.params.id }).then(function(){
			});
		};
		
		$scope.solicitarNegocio = function(){
			anuncioService.solicitarNegocio($state.params.id).then(function(result){
				
			});
		}
	}]);

	return app;
});