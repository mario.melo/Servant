define([], function() {

	var routes = [
		{
			module: 'consulta',
			text: '',
			controller:'consultaController',
			view: 'consulta', 
			state : {
				name : 'consulta',
				url: 'buscar/:uf'
			}
		},

		{
			module: 'consulta',
			text: '',
			controller:'anuncioUsuarioController',
			view: 'anuncio', 
			state : {
				name : 'consulta.anuncio',
				url: 'anuncio/:id'
			}
		}
		];

	return routes;
});