define(['app'], 
		function(app){

	app.factory("consultaService", ['$http','$q', '$rootScope', '$window', 'restFactory',
	                                function($http, $q, $rootScope, $window, restFactory){

		
		function getPath(path){
			return restFactory.resourceUrl.consulta + (path ? path : '');
		}

		var getAnuncio = function(id){
			return $http({
				method: 'GET',
				url: getPath("/anuncio/" + id)
			});
		};
		
		var spatialSearch = function(params){
			return $http({
				method: 'GET',
				url: getPath("/geo"),
				params: params
			});
		};
		
		var search = function(params){
			return $http({
				method: 'GET',
				url: getPath(),
				params: params
			});
		};
		
		var visualizar =  function(id){
			return $http({
				method: 'POST',
				url: getPath("/visualizado/" + id)
			});
		};
		
		
		var consultaOcupacoes =  function(id){
			return $http({
				method: 'GET',
				url: getPath("/ocupacoes"),
				params: {'id': id}
			});
		};
		
		var getUfs = function(){
			return $http.get(getPath("/ufs"));
		};
		
		return {
			getAnuncio : getAnuncio,
			spatialSearch: spatialSearch,
			search: search,
			visualizar: visualizar,
			consultaOcupacoes: consultaOcupacoes,
			getUfs: getUfs
		};
	}]);

	return app;

});