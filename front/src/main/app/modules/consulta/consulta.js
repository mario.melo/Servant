define(_createRoute([
    'modules/consulta/services/consultaService',
	'modules/comuns/directivas/paginacao',
	'modules/consulta/controllers/consultaController',
	'modules/comuns/services/anuncioService',
	'modules/consulta/controllers/anuncioUsuarioController'
	]), function(app){
	'use strict';
        
	return app;
});