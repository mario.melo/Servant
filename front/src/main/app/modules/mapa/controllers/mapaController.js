define([ 'app'  ], function(app) {
	app.controller('mapaController', ['$filter', '$state','$scope', '$stateParams', '$rootScope','$window', 'consultaService', '$location', '$localStorage',
		function($filter, $state, $scope, $stateParams, $rootScope, $window, consultaService, $location, $localStorage){


		hello.init({google: '1044895406871-4ekr4g0pnm5jf86c500uigup3mqi23se.apps.googleusercontent.com' });

		$scope.facebook = function() {
			$window.location.replace("/backend/api/public/facebook/signin");
		};

		$scope.google = function() {
			$window.location.replace("/backend/api/public/google/signin");
		};
		
		$scope.direcionarSeUfSelecionada = function(){
			if($localStorage.ufSelecionada){
				$location.path('/buscar/' + $localStorage.ufSelecionada);
			}
		}
		
		$scope.direcionarSeUfSelecionada();
		
		$scope.vm = {
				uf:null,
				carregarUfs: function(){

					if(!$localStorage.ufSelecionada){
						consultaService.getUfs().then(response=>{
							$scope.vm.ufs = response.data.resultado;
						});
					}
				},
				selecionarUf: function(){
					$localStorage.ufSelecionada = $scope.vm.uf;
					$location.path('/buscar/' + $scope.vm.uf);
				}
		}

		$scope.login = function(){			

			hello( 'google' ).login( function() {
				token = hello( 'google' ).getAuthResponse().access_token;
			})

//			hello('facebook').login().then(function() {
//			alert('You are signed in to Facebook');
//			}, function(e) {
//			alert('Signin error: ' + e.error.message);
//			});

		};

	}]);

	return app;
});
