define([], function() {
  var routes = [
                
      {
          module: 'mapa',
          text: 'Mapa',
          view: 'mapa',
          controller : 'mapaController',
          state : {
          	name : 'mapa',
        	  	url : 'mapa'
          },
//    		roles : [ '*' ]
        }
  ];
  return routes;
});