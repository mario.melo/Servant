define(['app'], 
		function(app){

	app.factory("homeService", ['$http','$q', 'restFactory', '$rootScope', 
	                                         function($http, $q, restFactory, $rootScope){

	
		
		var signin = function(value){
			return restFactory.google.one("signin").get();
		};
	

		return {
			signin: signin
		};
	}]);

	return app;

});