define(['app'], 
        function(app){
	
	app.factory("loginService", ['$http','$q', 'ResourcesService', '$rootScope', '$translatePartialLoader', '$window',
					 			function($http, $q, ResourcesService, $rootScope, $translatePartialLoader, $window){
		
		//I18n
		$translatePartialLoader.addPart('login');
		
		var socialSignin = function(uri){
			var q = $q.defer();
			
			//TODO: transformar o tamanho em constantes.. cada rede tem tamanho proprio
			//Exe:. https://blog.gisspan.com/2016/05/using-angular-promise-to-wrap-up-oauth.html https://searchcode.com/codesearch/view/84959059/
			var popup = $window.open(uri, '', "top=100,left=100,width=500,height=500");

			var popupChecker = $interval(function(){

				if (window.codeServantUser != undefined){
					q.resolve(window.codeServantUser);
					popup.close();
					$interval.cancel(popupChecker);
					$rootScope.setCurrentUser(window.codeServantUser);
					window.codeServantUser = undefined;
				}else if (popup.closed){
					$interval.cancel(popupChecker);
					q.reject();
				}
			}, 1000)

			return q.promise;
		};
		
		return {
			socialSignin : socialSignin
		};
	}]);
	
	return app;
		
});