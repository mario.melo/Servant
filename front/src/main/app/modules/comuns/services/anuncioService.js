
define(['app'], 
		function(app){

	app.factory("anuncioService", ['$http','$q', '$rootScope', '$window','restFactory',
		function($http, $q, $rootScope, $window, restFactory){

		function getPath(path){
			return restFactory.resourceUrl.anuncio + path;
		}

		var getOcupacoesPrimeiroNivel = function(){
			return $http({
				method: 'GET',
				url: getPath("/ocupacoes-primeiro-nivel")
			});
		};

		var getSubOcupacoes = function(id){

			if(!id){
				throw new Error("Attribute ID is requerid!")
			}
			return $http({
				method: 'GET',
				url: getPath("/ocupacoes-subnivel/" + id)
			});
		};

		var getMeusAnuncios= function(){
			return $http({
				method: 'GET',
				url: getPath("/meus-anuncios")
			});
		};

		var getMeusAnuncios= function(){
			return $http({
				method: 'GET',
				url: getPath("/meus-anuncios")
			});
		};
		
		var getRespostasSolicitacaoOrcamento= function(numberPage){
			return $http({
				method: 'GET',
				params:{'page': numberPage},
				url: getPath("/respostas-solicitacao-orcamento")
			});
		};

		var solicitarNegocio = function(id){
			return $http({
				method: 'POST',
				url: getPath("/solicitacao-negocio/" + id)
			});
		};
		
		var avaliarNegocioNegocio = function(data){
			return $http.post(getPath("/avaliar-solicitacao-servico"), data);
		};
		
		
		var responderSolicitacaoOrcamento = function(data){
			return $http.post(getPath("/responder-solicitacao-orcamento"), data);
		};


		var getNegociosPendentes = function(){
			return $http({
				method: 'GET',
				url: getPath("/negocios-pendentes")
			});
		};
		
		
		var getAnunciosAbertos = function(){
			return $http({
				method: 'GET',
				url: getPath("/orcamentos-abertos")
			});
		};
		

		var getDetalheServicosPendentes = function(){
			return $http({
				method: 'GET',
				url: getPath("/negocios-pendentes/detalhe/servicos")
			});
		};
		
		var getDetalheOrcamentosPendentes = function(){
			return $http({
				method: 'GET',
				url: getPath("/negocios-pendentes/detalhe/orcamentos")
			});
		};
		
		var solicitarOrcamento = function(data){
			return $http.post(getPath("/solicitar-orcamento"), data);
		};
		
		var getUfs = function(){
			return $http.get(getPath("/ufs"));
		};
		
		var getMunicipios = function(uf){
			return $http.get(getPath("/"+ uf+"/municipios"));
		};
		
		var getMunicipio = function(ibge){
			return $http.get(getPath("/municipio/ibge/"+ibge));
		};

		var salvar = function(anuncio, arquivos){
			return $http({
				'method': 'POST',
				'url':  getPath("/salvar"),
				'headers': {
					'Content-Type': undefined
				},
				'data': anuncio,
				'transformRequest': function (data) {
					var formData = new FormData();

					formData.append("dados", new Blob([JSON.stringify(data)], {type: "application/json"}));
					angular.forEach(arquivos, function (item) {
						formData.append('files', item);
					});
					return formData;
				}
			});
		};

		return {
			getOcupacoesPrimeiroNivel: getOcupacoesPrimeiroNivel,
			getSubOcupacoes: getSubOcupacoes,
			salvar: salvar,
			getMeusAnuncios: getMeusAnuncios,
			getPrivatePath:getPath,
			solicitarNegocio: solicitarNegocio,
			getNegociosPendentes: getNegociosPendentes,
			getDetalheServicosPendentes: getDetalheServicosPendentes,
			avaliarNegocioNegocio: avaliarNegocioNegocio,
			solicitarOrcamento: solicitarOrcamento,
			getDetalheOrcamentosPendentes: getDetalheOrcamentosPendentes,
			responderSolicitacaoOrcamento: responderSolicitacaoOrcamento,
			getRespostasSolicitacaoOrcamento: getRespostasSolicitacaoOrcamento,
			getUfs: getUfs,
			getMunicipios: getMunicipios,
			getMunicipio: getMunicipio,
			getAnunciosAbertos: getAnunciosAbertos
		};
	}]);
	return app;
});