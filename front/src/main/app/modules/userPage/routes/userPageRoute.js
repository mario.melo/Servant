define([], function() {

	var routes = [
		{
			module: 'userPage',
			text: '',
			controller:'userPageController',
			view: 'userPage', 
			state : {
				name : 'userPage',
				url: 'usuario'
			}
		},
		{
			module: 'userPage',
			text: '',
			controller:'procurarServicoController',
			view: 'procurarServico', 
			state : {
				name : 'userPage.procurarServico',
				url: 'procurar-servico'
			}
		},
		{
			module: 'userPage',
			text: '',
			controller:'novoAnuncioController',
			view: 'novoAnuncio', 
			state : {
				name : 'userPage.novoAnuncio',
				url: 'anuncio/novo'
			},

		}];

	return routes;
});