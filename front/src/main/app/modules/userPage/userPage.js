define(_createRoute([
    'modules/userPage/services/userPageService',
	'modules/userPage/controllers/userPageController',
	'modules/userPage/controllers/procurarServicoController',
	'modules/userPage/controllers/novoAnuncioController',
	'modules/comuns/services/anuncioService'
	]), function(app){
	'use strict';
        
	return app;
});