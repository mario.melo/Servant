define(['app'], 
		function(app){

	app.factory("userPageService", ['$http','$q', '$rootScope', '$window', 'restFactory', 'Restangular','$translatePartialLoader',
	                                function($http, $q, $rootScope, $window, restFactory, Restangular, $translatePartialLoader){

		//I18n
		$translatePartialLoader.addPart('login');

		var dadosUsuario = function(){
			$http.get('/backend/api/private/usuario/dadosUsuario').then();
			
		};
		
		var	buscarCep = function(cep){
			return $http.get('https://viacep.com.br/ws/'+ cep +'/json');
			
		}

		return {
			getDadosUsuario: dadosUsuario,
			buscarCep : buscarCep
		};
	}]);

	return app;

});