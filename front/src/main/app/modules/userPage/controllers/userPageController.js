define([
	'app'

	], function(app) {


	app.controller('userPageController', [ '$scope' ,'$rootScope','userPageService','anuncioService',
		function( $scope, $rootScope, userPageService, anuncioService){


		$scope.page = {
				respostasSolicitacaoOrcamento:[]
		};

		var aux = {};

		anuncioService.getNegociosPendentes().then(function(success){
			$scope.page.pendencias =  success.data.resultado;
		},function(erro){

		});

		anuncioService.getMeusAnuncios().then(function(success){
			$scope.page.meusAnuncios = success.data.resultado;
		},function(erro){

		});
		
		
		anuncioService.getAnunciosAbertos().then(function(success){
			$scope.page.anuncioAbertos = success.data.resultado;
		},function(erro){

		});

		$scope.detalheServicosPendentes = function(){
			if($scope.page.pendencias.solicitacoesServico > 0)
				anuncioService.getDetalheServicosPendentes().then(function(success){
					$scope.page.pendencias.solicitacoesPendentes = success.data.resultado;
				});
		};

		$scope.detalharRespostasSolicitacaoOrcamento = function(click){

			if((click && aux.page) || (aux.page && aux.page.last))
				return;

			if($scope.page.pendencias && $scope.page.pendencias.respostasOrcamento > 0){

				var number = aux.page && !aux.page.last ? aux.page.number + 1 : 0;

				anuncioService.getRespostasSolicitacaoOrcamento(number).then(function(success){
					aux.page = success.data.resultado;
					aux.page.content.forEach(function(value){
						$scope.page.respostasSolicitacaoOrcamento.push(value);
					});
				});
			}
		};

		$scope.detalheOrcamentosPendentes = function(){
			if($scope.page.pendencias.solicitacoesOrcamento > 0)
				anuncioService.getDetalheOrcamentosPendentes().then(function(success){
					$scope.page.pendencias.orcamentosPendentes = success.data.resultado;
				});
		};

		$scope.avaliarSolicitacaoNegocio = function(avaliacao){
			anuncioService.avaliarNegocioNegocio(avaliacao).then(function(success){
				$scope.page.pendencias.solicitacoesServico = $scope.page.pendencias.solicitacoesServico - 1;

				if($scope.page.pendencias.solicitacoesServico === 0){
					$scope.page.pendencias.solicitacoesPendentes = null;
				}else{
					var index = _.findIndex($scope.page.pendencias.solicitacoesPendentes, function(value){return value.id == avaliacao.id});
					$scope.page.pendencias.solicitacoesPendentes.splice(index, 1);
				}
			});
		};


		$scope.responderSolicitacaoOrcamento = function(solicitacao){
			anuncioService.responderSolicitacaoOrcamento(solicitacao).then(function(success){
				$scope.page.pendencias.solicitacoesOrcamento--;

				if($scope.page.pendencias.solicitacoesOrcamento === 0){
					$scope.page.pendencias.orcamentosPendentes = null;
				}else{
					var index = _.findIndex($scope.page.pendencias.orcamentosPendentes, function(value){return value.id == solicitacao.id});
					$scope.page.pendencias.orcamentosPendentes.splice(index, 1);
				}
			});
		};
	}]);

	return app;
});