define([
	'app'

	], function(app) {

	
	app.controller('procurarServicoController', [ '$scope' ,'$rootScope','userPageService', 'Upload','anuncioService','$timeout', 'NgMap', '$notificationService',
		function( $scope, $rootScope, userPageService, Upload, anuncioService, $timeout, NgMap, $notificationService){

		$scope.removerImagem = function(arquivos, index){
			if($scope.view.imagem == index){
				$scope.view.imagem = 0;
			}

			arquivos.splice(index, 1);
		}

		$scope.arquivos = null;

		$scope.view = {
				imagem: 0,
				passosLiberados:[1],
				markers: [],
				ufs:[],
				uf:null,
				munUf:{}
		
		};
		$scope.step = 1;

		$scope.carregarUfs = function(){
			anuncioService.getUfs().then(response => {
				$scope.view.ufs = response.data.resultado;
			});
		}
		
		$scope.$watch('view.uf', function(value, old){
			if(value != old){
				if($scope.view.munUf[$scope.view.uf]){
					if($scope.orcamento.endereco.cidade.uf.id == value){
						$scope.orcamento.endereco.cidade.id = $scope.orcamento.endereco.cidade.id;
					}
					return;
				}
				anuncioService.getMunicipios($scope.view.uf).then(response => {
					$scope.view.munUf[$scope.view.uf] = {};
					$scope.view.munUf[$scope.view.uf].municipios = response.data.resultado;
					if($scope.orcamento.endereco.cidade.uf.id == value){
						$scope.orcamento.endereco.cidade.id = $scope.orcamento.endereco.cidade.id;
					}
				});
			}
		});
		
		$scope.carregarMunicipios = function(){
			anuncioService.getUfs().then(response => {
				$scope.view.municipios = response.data.resultado;
			});
		}
		
		$scope.verificaPassoAtivo = function(passo){
			if($scope.view.passosLiberados.indexOf(passo) > -1){
				$scope.step = passo;
				return true;
			}
			$notificationService.$notify.error({title: 'Você ainda não preencheu todos os dados necessários!', message: 'Continue, falta pouco!'});
			return false;
		}

		$scope.validaForm = function(passo){
			if(this.formFixo.$valid){
				if($scope.view.passosLiberados.indexOf(passo)  == -1){
					$scope.view.passosLiberados.push(passo);
				}
				$scope.step = passo;
			}else {
				this.formFixo.$setSubmitted(true);
			}
		}


		var Ocupacao = function(_itens, _nivel, _pai){
			_itens.forEach(value=>{value.pai = _pai;});
			
			return{
				itens:_itens ? _itens : null,
						nivel:_nivel == 0 || _nivel  ? _nivel : null,
								pai: _pai ? _pai : null
			}
		};

		$scope.orcamento = {
				descricao:null,
				titulo:null,
				endereco:{}
		};

		$scope.ocupacoes = [];

		anuncioService.getOcupacoesPrimeiroNivel().then(function(success){
			$scope.ocupacoes.push(new Ocupacao(success.data.resultado, 0));

		},function(erro){

		});


		function _pop(index, list){
			if(list.length - 1 > index){
				list.pop();
				_pop(index, list);
			}
		}

		$scope.selecionarSubocupacoes = function(ocupacao, conjunto){
			$scope.orcamento.ocupacao = ocupacao;
			if(!(conjunto.nivel +1 < $scope.ocupacoes.length && ocupacao == $scope.ocupacoes[conjunto.nivel +1].pai)){
				_pop(conjunto.nivel, $scope.ocupacoes);
				anuncioService.getSubOcupacoes(ocupacao.id).then(function(success){
					$scope.ocupacoes.push(new Ocupacao(success.data.resultado, $scope.ocupacoes.length, ocupacao));
					if(success.data.resultado.length == 0){

						if($scope.view.passosLiberados.indexOf(2) < 0)
							$scope.view.passosLiberados.push(2);
						$timeout(function(){
							$scope.step = 2;
						},300);

					}
				});
			}else{
				$scope.step = 2;
			}

			conjunto.itens.forEach(function(value){
				value.ativo = false;
			});

			ocupacao.ativo = true;
		};

		$scope.selecionarLocal = function(local){
			$scope.view.markers = [];
			$scope.view.markers.push({lat:local.latLng.lat(), lng: local.latLng.lng()});
		};

		$scope.carregarPosicaoAtual = function(){
			var options = {
					enableHighAccuracy: true
			};

			navigator.geolocation.getCurrentPosition(function(pos) {
				$scope.$apply(function(){
					$scope.position = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
					//$scope.selecionarLocal($scope.position);

					//$scope.view.markers.push({lat:pos.coords.latitude, lng: pos.coords.longitude});

				});
			},
			function(error) {
				console.log(error);
			}, options);
		};

		NgMap.getMap().then(function(map) {
			$scope.map = map;
			console.log(map.getBounds().toString());

			var bounds = map.getBounds();
			$scope.carregarPosicaoAtual();
		});
		
		$scope.buscarCep = function(){
			
			if($scope.orcamento.endereco.cep.length != 9){
				$scope.orcamento.endereco.cep = '';
				return;
			}
			
			
			userPageService.buscarCep($scope.orcamento.endereco.cep).then(response =>{
				if(response.data.erro){
					$notificationService.$notify.warning({title: 'Você digitou certo?', message: 'Não encontramos seu CEP.'});
				}else{
					$scope.orcamento.endereco.endereco = response.data.logradouro;
					$scope.orcamento.endereco.bairro = response.data.bairro;
					
					anuncioService.getMunicipio(response.data.ibge).then(resp=>{
						$scope.view.uf = resp.data.resultado.uf.id;
						$scope.orcamento.endereco.cidade =  resp.data.resultado;
					});
					
				}
			}, erro =>{
				console.error(erro)
			});
		};
		
		
		
		$scope.solicitarOrcamento = function(solicitacao){
			anuncioService.solicitarOrcamento($scope.orcamento).then(function(){
			});
		};
		
		

//		$scope.salvar = function(arquivos){
//
//			$scope.orcamento.latitude = $scope.view.markers[0].lat;
//			$scope.$scope.orcamento.longitude = $scope.view.markers[0].lng;
//
//			if($scope.view.imagem && $scope.view.imagem != 0){
//
//				var tempFile = $scope.arquivos[0];
//
//				$scope.arquivos[0] = $scope.arquivos[$scope.view.imagem];
//
//				$scope.arquivos[$scope.view.imagem] = tempFile;
//			}
//
//			anuncioService.salvar($scope.orcamento, $scope.arquivos).then(function(){
//
//			});
//		};
	}]);

	return app;
});