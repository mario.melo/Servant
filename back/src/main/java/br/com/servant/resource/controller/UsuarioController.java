package br.com.servant.resource.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.servant.apresentacao.base.Resposta;
import br.com.servant.entidades.Anuncio;
import br.com.servant.negocio.seguranca.OAuthProvider;
import br.com.servant.negocio.seguranca.UsuarioComumToken;
import br.com.servant.negocio.service.UsuarioService;

@Controller
@RequestMapping("/api/private/usuario")
public class UsuarioController {

	private static final Log LOGGER = LogFactory.getLog(UsuarioController.class);
	
	@Autowired
	private OAuthProvider comumProvider;
	
	@Autowired
	private UsuarioService service;
	
	@RequestMapping(value = "/dadosUsuario", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> obterDadosUsuarioLogado(){
		LOGGER.info("obtendo informações do usuário autenticado");
		
		OAuth2Authentication auth = (OAuth2Authentication)SecurityContextHolder.getContext().getAuthentication();
		
		UsuarioComumToken user = (UsuarioComumToken)auth.getUserAuthentication();
		
		Resposta resp = new Resposta.Builder()
			.resultado(user)
			.build();
		
		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	@RequestMapping(value = "/criar-anuncio", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> logout(@RequestBody Anuncio anuncio){
		
		service.createAnuncio(anuncio);
		
		Resposta resp = new Resposta.Builder()
			.build();

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
}