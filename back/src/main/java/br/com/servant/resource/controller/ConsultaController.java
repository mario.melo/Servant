package br.com.servant.resource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.servant.apresentacao.base.Resposta;
import br.com.servant.negocio.service.AnuncioService;
import br.com.servant.negocio.service.ConsultaAnuncioIndexService;
import br.com.servant.negocio.service.OcupacaoService;
import lombok.Getter;
import lombok.Setter;

@Controller
@RequestMapping("/api/public/anuncio")
public class ConsultaController {

	@Autowired
	private ConsultaAnuncioIndexService consulta;
	
	@Autowired
	private AnuncioService anuncioService;
	
	@Autowired
	private OcupacaoService ocupacaoService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> consultar(Filter filter){

		Resposta resp = new Resposta.Builder()
				.resultado(consulta.consultar(filter))
				.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	@RequestMapping(value = "ocupacoes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> ocupacao(@RequestParam(name = "id", required = false) Long pai){

		Resposta resp = new Resposta.Builder()
				.resultado(pai != null ? ocupacaoService.findSubOcupacoes(pai) : ocupacaoService.findOcupacoesFirstLevel())
				.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	@RequestMapping(value="/geo" ,method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> geoConsulta(Filter filter){

		Resposta resp = new Resposta.Builder()
				.resultado(consulta.geoConsulta(filter))
				.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	@RequestMapping(value="/visualizado/{id}" ,method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> visualizado(@PathVariable("id") Long id){

		anuncioService.visualizado(id);

		return new ResponseEntity<Resposta>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/anuncio/{id}" ,method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> getAnuncio(@PathVariable("id")Long id){

		Resposta resp = new Resposta.Builder()
				.resultado(anuncioService.getAnuncio(id))
				.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}

	@Getter
	@Setter
	public static class Filter{

		private String termo;
		private Integer page;
		private Double latitude;
		private Double longitude;
		private Double radius;
		private Long ocupacao;
		private String uf;
		
	}
	
	@RequestMapping(value = "/ufs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> ufs(){

		Resposta resp = new Resposta.Builder()
		.resultado(ocupacaoService.ufs())
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
}
