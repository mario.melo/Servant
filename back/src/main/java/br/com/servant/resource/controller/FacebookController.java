package br.com.servant.resource.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import br.com.servant.apresentacao.base.Resposta;
import br.com.servant.configuration.SocialConfiguration.SocialService;
import br.com.servant.entidades.Usuario;
import br.com.servant.negocio.seguranca.OAuthProvider;
import br.com.servant.negocio.service.UsuarioService;

import com.github.scribejava.core.model.Token;
import com.github.scribejava.core.oauth.OAuthService;

@RestController
@RequestMapping("/api/public/facebook")
public class FacebookController<FacebookApi> {

	private static final Logger LOGGER = LoggerFactory	.getLogger(FacebookController.class);
	private static final String FACEBOOK = "facebook";

	@Autowired
	private FacebookConnectionFactory facebookConnectionFactory;

	@Autowired
	private SocialService servantFacebookSocialService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private OAuthProvider comumProvider;



	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public ModelAndView signin(HttpServletRequest request, HttpServletResponse response) throws Exception {

		OAuth2Operations oauthOperations = facebookConnectionFactory.getOAuthOperations();


		String authorizeUrl = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, servantFacebookSocialService.getOAuth2Parameters());
		RedirectView redirectView = new RedirectView(authorizeUrl, true, true, true);

		return new ModelAndView(redirectView);
	}

	@RequestMapping(value = "/callback", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Resposta> autenticar(@RequestParam("code") String code,
			@RequestParam("state") String state, HttpServletRequest request, HttpServletResponse response) throws Exception {

		OAuthService oAuthService = servantFacebookSocialService.getOAuthService();

		//Verifier verifier = new Verifier(code);
		//Token accessToken = oAuthService.getAccessToken(Token.empty(), verifier);

		//FacebookTemplate template = new FacebookTemplate(accessToken.getToken());

		//FacebookProfile facebookProfile = template.userOperations().getUserProfile();

//		Usuario user = usuarioService.getUsuario(facebookProfile);
//
//		comumProvider.socialAuthenticate(user);
//
//		Resposta resp = new Resposta.Builder()
//		.resultado(comumProvider.getUsuarioLogado())
//		.build();
//
//		HttpHeaders responseHeaders = new HttpHeaders();
//		responseHeaders.add("token", comumProvider.getToken());

//		return new ResponseEntity<Resposta>(resp, responseHeaders, HttpStatus.OK);
		
		return null;
	}

	//	@RequestMapping(value = "/callback", params = "error_reason", method = RequestMethod.GET)
	//	@ResponseBody
	//	public void error(@RequestParam("error_reason") String errorReason,
	//			@RequestParam("error") String error,
	//			@RequestParam("error_description") String description,
	//			HttpServletRequest request, HttpServletResponse response)
	//			throws Exception {
	//		try {
	//			LOGGER.error(
	//					"Error occurred while validating user, reason is : {}",
	//					errorReason);
	//			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, description);
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}
}
