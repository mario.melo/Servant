
package br.com.servant.resource.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.servant.negocio.service.SearchService;
import br.com.servant.apresentacao.base.Resposta;

@Controller
@RequestMapping("/api/public/search")
public class SearchController {

	private static final Log LOGGER = LogFactory.getLog(SearchController.class);

	@Autowired
	private SearchService service;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> ocupacoes(@RequestParam String term){
		
		service.consultarAnucnio(term);
		
		Resposta resp = new Resposta.Builder()
		//.resultado(ocupacaoService.findOcupacoesFirstLevel())
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}

}