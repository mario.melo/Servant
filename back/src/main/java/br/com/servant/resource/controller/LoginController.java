package br.com.servant.resource.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.servant.apresentacao.base.Resposta;
import br.com.servant.dtos.UsuarioDTO;
import br.com.servant.negocio.seguranca.OAuthProvider;
import br.com.servant.negocio.seguranca.UsuarioComumToken;


@Controller
@RequestMapping("/api/private/login")
public class LoginController {

	private static final Log LOGGER = LogFactory.getLog(LoginController.class);

	@Autowired
	private TokenStore redisTokenStore;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private OAuthProvider comumProvider;

	@RequestMapping(value = "/dadosUsuarioLogado", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> obterDadosUsuarioLogado(){
		LOGGER.info("obtendo informações do usuário autenticado");

		OAuth2Authentication auth = (OAuth2Authentication)SecurityContextHolder.getContext().getAuthentication();

		if(auth != null){
			UsuarioComumToken user = (UsuarioComumToken)auth.getUserAuthentication();

			Resposta resp = new Resposta.Builder()
					.resultado(converterUsuario(user))
					.build();

			return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
		}

		return new ResponseEntity<Resposta>(HttpStatus.UNAUTHORIZED);
	}

//	@RequestMapping(value = "/logout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<Resposta> logout(){
//
//		LOGGER.info("Logout do usuário");comumProvider.getUsuarioLogado()
//		SecurityContextHolder.clearContext();
//		redisTokenStore.removeAccessToken(comumProvider.getOAuth2AccessToken());
//
//		Resposta resp = new Resposta.Builder()
//				.build();
//
//		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
//	}

	public UsuarioDTO converterUsuario(UsuarioComumToken user) {

		UsuarioDTO dto = new UsuarioDTO();

		UsuarioDTO.Usuario usuario = new UsuarioDTO().new Usuario();
		usuario.setId(user.getIdUsuarioAutenticado());
		usuario.setNome(user.getNome());
		//		usuario.setCpf(user.getCpf());
		usuario.setEmail(user.getEmail());
		usuario.setTelefone(user.getTelefone());
		usuario.setRoles(converterRoles(user.getAuthorities()));

		dto.setUsuario(usuario);
		return dto;
	}

	private List<String> converterRoles(Collection<GrantedAuthority> authorities) {

		List<String> retorno = new ArrayList<String>();
		if(authorities != null){
			for (GrantedAuthority grantedAuthority : authorities) {
				retorno.add(grantedAuthority.getAuthority());
			}
		}
		return retorno;
	}

}