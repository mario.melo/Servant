
package br.com.servant.resource.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.servant.apresentacao.base.Resposta;
import br.com.servant.dtos.NegociosPendentesDTO;
import br.com.servant.entidades.Anuncio;
import br.com.servant.entidades.Orcamento;
import br.com.servant.negocio.service.AnuncioService;
import br.com.servant.negocio.service.OcupacaoService;

@RestController
@RequestMapping("/api/private/anuncios")
public class AnuncioController {

	@Autowired
	private AnuncioService anuncioService;

	@Autowired
	private OcupacaoService ocupacaoService;

	@RequestMapping(value = "/ocupacoes-primeiro-nivel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> ocupacoes(){

		Resposta resp = new Resposta.Builder()
		.resultado(ocupacaoService.findOcupacoesFirstLevel())
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}

	@RequestMapping(value = "/ufs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> ufs(){

		Resposta resp = new Resposta.Builder()
		.resultado(ocupacaoService.ufs())
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{uf}/municipios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> municipios(@PathVariable String uf){
		Resposta resp = new Resposta.Builder()
		.resultado(ocupacaoService.getMunicipios(uf))
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	@RequestMapping(value = "/municipio/ibge/{ibge}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> municipios(@PathVariable Integer ibge){
		Resposta resp = new Resposta.Builder()
		.resultado(ocupacaoService.getMunicipio(ibge))
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}

	@RequestMapping(value = "/ocupacoes-subnivel/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> ocupacoeso(@PathVariable("id") Long id){

		Resposta resp = new Resposta.Builder()
		.resultado(ocupacaoService.findSubOcupacoes(id))
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/orcamentos-abertos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> abertos(){

		Resposta resp = new Resposta.Builder()
		.resultado(ocupacaoService.getOrcamentosAbertos())
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}

	@RequestMapping(value="/salvar",  method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>  salvarDadosTarefa(  @RequestPart("dados") Anuncio anuncio, 
			@RequestPart("files")List<MultipartFile> files) {

		anuncioService.salvar(anuncio, files);

		return new ResponseEntity<>( HttpStatus.OK);
	}
	
	@RequestMapping(value="/solicitacao-negocio/{id}",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> solicitacaoNegocio(@PathVariable("id") Long id) {
		
		anuncioService.solicitarNegocio(id);

		return new ResponseEntity<>( HttpStatus.OK);
	}
	
	@RequestMapping(value="/solicitar-orcamento",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> solicitarOrcamento(@RequestBody Orcamento orcamento) {
		
		anuncioService.solicitarOrcamento(orcamento);

		return new ResponseEntity<>( HttpStatus.OK);
	}
	
	@RequestMapping(value="/negocios-pendentes",  method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta>  negociosPendentes() {
		
		Resposta resp = new Resposta.Builder()
				.resultado(anuncioService.negociosPendentes())
				.build();	
		
		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	@RequestMapping(value = "/negocios-pendentes/detalhe/servicos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> getDetalheNegociosPendentes(){

		Resposta resp = new Resposta.Builder()
		.resultado(anuncioService.getDetalheServicosPendentes())
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}
	
	@RequestMapping(value = "/negocios-pendentes/detalhe/orcamentos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> getDetalheOrcamentosPendentes(){

		Resposta resp = new Resposta.Builder()
		.resultado(anuncioService.getDetalheOrcamentosPendentes())
		.build();	

		return new ResponseEntity<Resposta>(resp,  HttpStatus.OK);
	}

	@RequestMapping(value = "/meus-anuncios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> getAnuncios(){

		Resposta resp = new Resposta.Builder()
		.resultado(anuncioService.getAnunciosUsuarioLogado())
		.build();	

		return new ResponseEntity<Resposta>(resp, HttpStatus.OK);
	}

	@RequestMapping(value = "/avaliar-solicitacao-servico", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> aceitaSolicitacaoServico(@RequestBody NegociosPendentesDTO negocio){
		
		anuncioService.avaliarPedidoNegocio(negocio);

		return new ResponseEntity<Resposta>(HttpStatus.OK);
	}

	@RequestMapping(value = "/responder-solicitacao-orcamento", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> responderSolicitacaoOrcamento(@RequestBody NegociosPendentesDTO negocio){
		
		anuncioService.responderSolicitacaoOrcamento(negocio);

		return new ResponseEntity<Resposta>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/respostas-solicitacao-orcamento", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Resposta> getRespostasOrcamento(@RequestParam("page")Integer page){

		Resposta resp = new Resposta.Builder()
		.resultado(anuncioService.getRespostasSolicitacaoOrcamento(page))
		.build();	

		return new ResponseEntity<Resposta>(resp, HttpStatus.OK);
	}
}