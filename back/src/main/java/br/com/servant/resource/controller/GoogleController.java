//package br.com.servant.resource.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.social.google.connect.GoogleConnectionFactory;
//import org.springframework.social.oauth2.OAuth2Parameters;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.ModelAndView;
//
//import br.com.servant.apresentacao.base.OAuthServiceConfig;
//import br.com.servant.entidades.Usuario;
//import br.com.servant.negocio.seguranca.OAuthProvider;
//import br.com.servant.negocio.service.UsuarioService;
//import br.com.servant.persistencia.repository.impl.UsuarioMongoRepository;
//
//@Controller
//@RequestMapping("/api/public/google")
//public class GoogleController<GoogleAPI> {
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(GoogleController.class);
//
//	@Autowired
//	private GoogleConnectionFactory googleConnectionFactory;
//
//	@Autowired
//	private OAuth2Parameters oAuth2ParametersGoogle;
//
//	@Autowired
//	private UsuarioMongoRepository repository;
//
//	@Autowired
//	@Qualifier("googleServiceProvider")
//	private OAuthServiceProvider<GoogleAPI> googleServiceProvider;
//	
//	@Autowired
//	private OAuthServiceConfig<GoogleAPI> googleServiceConfig;
//
//	@Autowired
//	private UsuarioService usuarioService;
//
//	@Autowired
//	private OAuthProvider comumProvider;
//	
//	@Autowired
//	private LoginController loginController;
//
//	@RequestMapping(value = "/signin", method = RequestMethod.GET)
//	public ModelAndView signin(HttpServletRequest request, HttpServletResponse response) throws Exception {
//
//		OAuth2Operations oauthOperations = googleConnectionFactory.getOAuthOperations();
//		oAuth2ParametersGoogle.setState("recivedfromgoogletoken");
//		
//		oAuth2ParametersGoogle.setScope(googleServiceConfig.getScope());
//		oAuth2ParametersGoogle.setRedirectUri(googleServiceConfig.getCallback());
//		String authorizeUrl = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, oAuth2ParametersGoogle);
//		RedirectView redirectView = new RedirectView(authorizeUrl, true, true, true);
//
//		return new ModelAndView(redirectView);
//	}
//
//	@RequestMapping(value = "/callback", method = RequestMethod.GET)
//	@ResponseBody
//	public ResponseEntity<Resposta> autenticar(@RequestParam("code") String code,
//			@RequestParam("state") String state, HttpServletRequest requestt, HttpServletResponse responsee) throws Exception {
//
//		OAuthService oAuthService = googleServiceProvider.getService();
//
//		Verifier verifier = new Verifier(code);
//		Token accessToken = oAuthService.getAccessToken(Token.empty(), verifier);
//
//		OAuthRequest request = new OAuthRequest(Verb.GET,"https://www.googleapis.com/oauth2/v2/userinfo?alt=json");
//		oAuthService.signRequest(accessToken, request);
//
//		Google template = new GoogleTemplate(accessToken.getToken());
//
//		Person person = template.plusOperations().getGoogleProfile();
//
//		Usuario user = usuarioService.getUsuario(person);
//
//		comumProvider.socialAuthenticate(user);
//		
//		OAuth2Authentication auth = (OAuth2Authentication)SecurityContextHolder.getContext().getAuthentication();
//
//		UsuarioComumToken userLogado = (UsuarioComumToken)auth.getUserAuthentication();
//
//		Resposta resp = new Resposta.Builder()
//		.resultado(loginController.converterUsuario(userLogado))
//		.build();
//
//		HttpHeaders responseHeaders = new HttpHeaders();
//		responseHeaders.add("token", comumProvider.getToken());
//
//		return new ResponseEntity<Resposta>(resp, responseHeaders, HttpStatus.OK);
//
//	}
//
//	//	@RequestMapping(value = "/callback", params = "error_reason", method = RequestMethod.GET)
//	//	@ResponseBody
//	//	public void error(@RequestParam("error_reason") String errorReason,
//	//			@RequestParam("error") String error,
//	//			@RequestParam("error_description") String description,
//	//			HttpServletRequest request, HttpServletResponse response)
//	//			throws Exception {
//	//		try {
//	//			LOGGER.error(
//	//					"Error occurred while validating user, reason is : {}",
//	//					errorReason);
//	//			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, description);
//	//		} catch (Exception e) {
//	//			e.printStackTrace();
//	//		}
//	//	}
//
//}
