package br.com.servant;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fodase {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		
		Map<String, String> ufs = new HashMap();
		ufs.put("Acre" 						,"AC");		 
		ufs.put("Alagoas" 					,"AL");
		ufs.put("Amapá"						,"AP");
		ufs.put("Amazonas"					,"AM");
		ufs.put("Bahia"						,"BA");
		ufs.put("Ceará"						,"CE");
		ufs.put("Distrito Federal"			,"DF");
		ufs.put("Espírito Santo" 			,"ES");
		ufs.put("Goiás"						,"GO");
		ufs.put("Maranhão"					,"MA");
		ufs.put("Mato Grosso"				,"MT");
		ufs.put("Mato Grosso do Sul"		,"MS");
		ufs.put("Minas Gerais"				,"MG");
		ufs.put("Pará"						,"PA");
		ufs.put("Paraíba"					,"PB");
		ufs.put("Paraná"					,"PR");
		ufs.put("Pernambuco"				,"PE");
		ufs.put("Piauí"						,"PI");
		ufs.put("Rio de Janeiro"			,"RJ");
		ufs.put("Rio Grande do Norte"		,"RN");
		ufs.put("Rio Grande do Sul"			,"RS");
		ufs.put("Rondônia"					,"RO");
		ufs.put("Roraima"					,"RR");
		ufs.put("Santa Catarina"			,"SC");
		ufs.put("São Paulo" 				,"SP");
		ufs.put("Sergipe"					,"SE");
		ufs.put("Tocantins"					,"TO");
		
		List<List<String>> records = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("/home/mario/Imagens/teste.csv"))) {
		    String line;
		    
		    FileWriter fw = new FileWriter("/home/mario/Imagens/script.sql");
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(";");
		        records.add(Arrays.asList(values));
		        String linha = "INSERT INTO servant.cidade(id, uf_sigla, ibge, nome ) VALUES(default,  '" + ufs.get(values[0]) + "', " + values[1] +", '"+  values[2].replace("'", "''") + "');";
		        fw.write(linha + System.lineSeparator());
		        
		    }
		    
		    fw.close();
		}
		
		
	}

}
