package br.com.servant.persistencia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.servant.base.CustomRepository;
import br.com.servant.entidades.Orcamento;

@Repository
public interface OrcamentoRepository extends CustomRepository<Orcamento, Long> {

	@Query(nativeQuery = true,name = "getOrcamentosAbertos" )
	List<Orcamento> getOrcamentosAbertos(@Param("usuario")Long usuario);	
}