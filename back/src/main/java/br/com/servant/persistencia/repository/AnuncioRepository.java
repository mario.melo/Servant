package br.com.servant.persistencia.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.servant.base.CustomRepository;
import br.com.servant.dtos.AnuncioDTO;
import br.com.servant.dtos.DetalheAnuncioDTO;
import br.com.servant.dtos.MeusAnunciosDTO;
import br.com.servant.dtos.NegociosPendentesDTO;
import br.com.servant.dtos.OrcamentoDTO;
import br.com.servant.entidades.Anuncio;

@Repository
public interface AnuncioRepository extends CustomRepository<Anuncio, Long> {
	
	@Query(name="getAnunciosUsuarioLogado", nativeQuery=true)
	List<MeusAnunciosDTO> getAnunciosUsuarioLogado(Long id);
	
	@Query(name="getDetalheAnuncio", nativeQuery=true)
	DetalheAnuncioDTO getDetalheAnuncio(@Param("id")Long id);
	
	@Query(name="getAnuncioComDadosLocalizacao", nativeQuery=true)
	List<AnuncioDTO> getAnuncioComDadosLocalizacao(@Param("ids") List<Long>ids);

	@Query(value="SELECT arquivo.base64 FROM servant.anuncio anuncio "
			+ "INNER JOIN servant.arquivo arquivo on arquivo.anuncio_id = anuncio.id WHERE anuncio.id = :id",  nativeQuery=true)
	List<String> getImagensAnuncio(@Param("id")Long id);
	
	@Query(name="getAnunciosById", nativeQuery=true)
	List<AnuncioDTO> getAnunciosById(@Param("ids")List<Long> ids);
	
	@Modifying
	@Query(name="visualizarAnuncio", nativeQuery=true)
	void visualizarAnuncio(@Param("id")Long id);
	
	@Query(name="getNegociosPendentes", nativeQuery=true)
	NegociosPendentesDTO getNegociosPendentes(@Param("id")Long id);
	
	@Query(name="getDetalheSolicitacaoServico", nativeQuery=true)
	List<NegociosPendentesDTO> getDetalheSolicitacaoServico(@Param("id")Long id);
	
	@Query(name="getDetalheSolicitacaoOrcamento", nativeQuery=true)
	List<NegociosPendentesDTO> getDetalheSolicitacaoOrcamento(@Param("id")Long id);
	
	@Query(name="getRespostasSolicitacaoOrcamento", countName="countRespostasSolicitacaoOrcamento", nativeQuery=true)
	Page<OrcamentoDTO> getRespostasSolicitacaoOrcamento(@Param("id")Long id, Pageable page);
	 
	@Query(value = "SELECT  id_usuario  = :idUsuario FROM servant.anuncio  WHERE id = :idAnuncio", nativeQuery = true)
	Boolean validarIdUsuario(@Param("idUsuario")Long idUsuario, @Param("idAnuncio")Long idAnuncio);
	
}
