package br.com.servant.persistencia.repository;

import org.springframework.stereotype.Repository;

import br.com.servant.base.CustomRepository;
import br.com.servant.entidades.Endereco;

@Repository
public interface EnderecoRepository extends CustomRepository<Endereco, Long> {
	
}
