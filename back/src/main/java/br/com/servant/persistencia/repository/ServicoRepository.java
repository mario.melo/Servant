package br.com.servant.persistencia.repository;

import org.springframework.stereotype.Repository;

import br.com.servant.base.CustomRepository;
import br.com.servant.entidades.Servico;

@Repository
public interface ServicoRepository extends CustomRepository<Servico, Long> {
	
}
