package br.com.servant.persistencia.repository;

import org.springframework.stereotype.Repository;

import br.com.servant.base.CustomRepository;
import br.com.servant.entidades.Arquivo;

@Repository
public interface ArquivoRepository extends CustomRepository<Arquivo, Long> {
	

}