package br.com.servant.persistencia.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.servant.base.CustomRepository;
import br.com.servant.entidades.Cidade;
import br.com.servant.entidades.Ocupacao;
import br.com.servant.entidades.UF;

@Repository
public interface OcupacaoRepository extends CustomRepository<Ocupacao, Long> {

	@Query("SELECT o FROM Ocupacao o WHERE o.ocupacao is null order by o.ordemExibicao ASC")
	List<Ocupacao> findOcupacaoFirstLevel();

	@Query("SELECT o FROM Ocupacao o WHERE o.idOcupacaoPai = :id order by o.ordemExibicao ASC")
	List<Ocupacao> findSubOcupacoes(@Param("id") Long id);
	
	@Query("SELECT u FROM UF u ORDER BY u.id")
	List<UF> getUFs();
	
	@Query("SELECT i FROM Cidade i WHERE i.uf.id = :uf ORDER BY i.nome")
	List<Cidade>getMunicipios(@Param("uf")String uf);

	
	@Query("SELECT i FROM Cidade i WHERE i.ibge = :ibge")
	Cidade getMunicipio(@Param("ibge")Integer ibge);

}
