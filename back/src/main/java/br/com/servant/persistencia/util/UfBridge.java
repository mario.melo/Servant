package br.com.servant.persistencia.util;

import org.hibernate.search.bridge.TwoWayStringBridge;

import br.com.servant.entidades.UF;

public class UfBridge  implements TwoWayStringBridge {

	@Override
	public String objectToString(Object object) {

		if(object == null)
			return null;

		if(object instanceof UF)
			return ((UF)object).getId();

		return object.toString();
	}

	@Override
	public Object stringToObject(String stringValue) {
		return UF.builder().id(stringValue).build();

	}
}