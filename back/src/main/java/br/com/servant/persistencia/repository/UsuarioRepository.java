package br.com.servant.persistencia.repository;

import org.springframework.stereotype.Repository;

import br.com.servant.dominio.TipoUsuarioEnum;
import br.com.servant.entidades.Usuario;
import br.com.servant.base.CustomRepository;

@Repository
public interface UsuarioRepository extends CustomRepository<Usuario, Long>  {
	
	Usuario findBySocialIdAndTipoUsuarioEnum(String socialId, TipoUsuarioEnum tipo);
	
	Usuario findByLoginAndSenha(String username, String senha);

}
