package br.com.servant.dtos;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class AnuncioDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	protected Long id;
	
	@Getter
	@Setter
	protected String descricao;
	
	@Getter
	@Setter
	protected String titulo;
	
	@Getter
	@Setter
	protected String ocupacao;
	
	@Getter
	@Setter
	protected Date dataCadastro;
	
	@Getter
	@Setter
	protected String  thumbnail;
	
	@Getter
	@Setter
	protected Double latitude;
	
	@Getter
	@Setter
	protected Double longitude;

	public AnuncioDTO(Long id, String titulo, String descricao, Date dataCadastro, String ocupacao, String thumbnail) {
		this.id = id;
		this.descricao = descricao;
		this.titulo = titulo;
		this.dataCadastro = dataCadastro;
		this.thumbnail = thumbnail;
		this.ocupacao = ocupacao;
	}
	
	public AnuncioDTO(Long id, String titulo, String descricao, Date dataCadastro, String ocupacao, Double latitude, Double longitude, String thumbnail) {
		this.id = id;
		this.descricao = descricao;
		this.titulo = titulo;
		this.dataCadastro = dataCadastro;
		this.thumbnail = thumbnail;
		this.ocupacao = ocupacao;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public AnuncioDTO(){}
}
