package br.com.servant.dtos;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class DetalheAnuncioDTO extends AnuncioDTO{
	
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private List<String> imagens;

	@Getter
	@Setter
	private String telefone;
	
	@Getter
	@Setter
	private String email;
	
	public DetalheAnuncioDTO(Long id, String titulo, String descricao, Date dataCadastro, String ocupacao, Double latitude, Double longitude, String thumbnail, String telefone, String email) {
		this.id = id;
		this.descricao = descricao;
		this.titulo = titulo;
		this.dataCadastro = dataCadastro;
		this.telefone = telefone;
		this.ocupacao = ocupacao;
		this.latitude = latitude;
		this.longitude = longitude;
		this.email = email;
		this.thumbnail = thumbnail;
	}
	
	public DetalheAnuncioDTO(){}
}
