package br.com.servant.dtos;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MeusAnunciosDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	private Long id;
	
	@Getter
	@Setter
	private String descricao;
	
	@Getter
	@Setter
	private String titulo;
	
	@Getter
	@Setter
	private String ocupacao;
	
	@Getter
	@Setter
	private Date dataCadastro;
	
	@Getter
	@Setter
	private Long visualizacoes;
	
}
