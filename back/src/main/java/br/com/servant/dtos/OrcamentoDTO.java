package br.com.servant.dtos;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class OrcamentoDTO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	private Date dataSolicitacao;

	@Getter
	@Setter
	private String pedido;

	@Getter
	@Setter
	private Date dataResposta;

	@Getter
	@Setter
	private String resposta;

	@Getter
	@Setter
	private String tituloAnuncio;

	@Getter
	@Setter
	private String idAnuncio;

	public OrcamentoDTO(){}

	public OrcamentoDTO(Long id, Date dataSolicitacao, String pedido, Date dataResposta, String resposta, 
			String tituloAnuncio, String idAnuncio) {
		super();
		this.id = id;
		this.dataSolicitacao = dataSolicitacao;
		this.pedido = pedido;
		this.dataResposta = dataResposta;
		this.resposta = resposta;
		this.tituloAnuncio = tituloAnuncio;
		this.idAnuncio = idAnuncio;
	}
}
