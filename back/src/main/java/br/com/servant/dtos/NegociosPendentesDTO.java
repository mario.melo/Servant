package br.com.servant.dtos;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NegociosPendentesDTO  implements Serializable{
	
	private static final long serialVersionUID = -8332344821972052123L;
	
	@Getter
	@Setter
	private Integer solicitacoesServico;
	
	@Getter
	@Setter
	private Integer solicitacoesOrcamento;
	
	@Getter
	@Setter
	private Integer respostasOrcamento;
	
	@Setter
	@Getter
	private Integer respostaServicosAceitos;
	
	@Getter
	@Setter
	private Long id;
	
	@Getter
	@Setter
	private String mensagemCliente;
	
	@Getter
	@Setter
	private Date dataSolicitacao;
	
	@Getter
	@Setter
	private Date dataResposta;
	
	@Getter
	@Setter
	private String nomeUsuario;
	
	@Getter
	@Setter
	private Boolean aceita;
	
	@Getter
	@Setter
	private String titulo;
	
	
	@Getter
	@Setter
	private String resposta;
	
	
	@Getter
	@Setter
	private Long idAnuncio;

	public NegociosPendentesDTO(Integer solicitacoesServico, Integer solicitacoesOrcamento, Integer respostasOrcamento, Integer respostaServicosAceitos) {
		this.solicitacoesServico = solicitacoesServico;
		this.solicitacoesOrcamento = solicitacoesOrcamento;
		this.respostasOrcamento =  respostasOrcamento;
		this.respostaServicosAceitos = respostaServicosAceitos;
	}
	

	public NegociosPendentesDTO(Long id, String mensagemCliente, Date dataSolicitacao, String nomeUsuario) {
		this.id = id;
		this.mensagemCliente = mensagemCliente;
		this.dataSolicitacao = dataSolicitacao;
		this.nomeUsuario = nomeUsuario;
	}
	

	public NegociosPendentesDTO(Long id, String mensagemCliente, String resposta, Date dataResposta, String titulo) {
		this.id = id;
		this.mensagemCliente = mensagemCliente;
		this.dataResposta = dataResposta;
		this.titulo = titulo;
		this.resposta = resposta;
		
	}

	public NegociosPendentesDTO(Long id, String mensagemCliente, String titulo, Long idAnuncio, Date dataSolicitacao, String nomeUsuario) {
		this.id = id;
		this.mensagemCliente = mensagemCliente;
		this.dataSolicitacao = dataSolicitacao;
		this.nomeUsuario = nomeUsuario;
		this.titulo = titulo;
		this.idAnuncio = idAnuncio;
	}
}