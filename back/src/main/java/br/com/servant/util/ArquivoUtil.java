package br.com.servant.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import br.com.servant.entidades.Anuncio;
import br.com.servant.entidades.Arquivo;
import net.coobird.thumbnailator.Thumbnails;

@Component
public class ArquivoUtil {

	@Value("${servant.uploadPath}")
	private String pathDiretorioImagem;


	public List<Arquivo> salvarArquivosFisico(Long id, List<MultipartFile> files, Anuncio anuncio) {


		List<Arquivo> result = new ArrayList<Arquivo>();

		String path = new String(pathDiretorioImagem).
				concat(id.toString()).concat("/").concat(LocalDateTime.now().toString());

		if(criarRelatorioRaiz(path)){
			try {
				File destino = null;
				String pathDestino = null;
				for(MultipartFile file: files){
					pathDestino = new String(path).concat("/").concat(file.getOriginalFilename());
					destino = new File(pathDestino);
					String thumbnail =generateThumbnail(file);
					String base64 = ArquivoUtil.base64Encoder(file.getBytes());
					file.transferTo(destino);
					result.add(new Arquivo(pathDestino, anuncio, new Date(), base64, thumbnail));
				}
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
		}

		return result;
	}
	
	private static String generateThumbnail(MultipartFile file){
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			Thumbnails.of(file.getInputStream())
			.size(100, 100)
			.outputQuality(1)
			.outputFormat("png")
			.toOutputStream(baos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return base64Encoder(baos.toByteArray());
	}

	public static String base64Encoder(String imagePath) {
		File file = new File(imagePath);
		return base64Encoder(file);
	}
	
	public static void Base64Decoder(String base64Image, String pathFile) {
		try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
			byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
			imageOutFile.write(imageByteArray);
		} catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
		}
	}

	public static String base64Encoder(File file) {
		String base64Image = "";
		try (FileInputStream imageInFile = new FileInputStream(file)) {
			byte imageData[] = new byte[(int) file.length()];
			imageInFile.read(imageData);
			base64Image = Base64.getEncoder().encodeToString(imageData);
		} catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
		}
		return base64Image;
	}
	
	public static String base64Encoder(byte[] file) {
		return Base64.getEncoder().encodeToString(file);
	}

	private boolean criarRelatorioRaiz(String path){
		File file = new File(path);
		return file.mkdirs();
	}
}
