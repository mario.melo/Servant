package br.com.servant.util;

import java.util.List;
import java.util.Map;

import org.hibernate.search.query.facet.Facet;

import lombok.Getter;
import lombok.Setter;

public class Page {

	@Setter
	@Getter
	private Map<String, String> sorting;

	@Setter
	@Getter
	private Integer count;

	@Setter
	@Getter
	private Integer page;

	@Setter
	@Getter
	private Integer total;

	@Setter
	@Getter
	private List<?>content;
	
	@Setter
	@Getter
	private List<Facet> facets;

	public Page() {

	}
	
	public Page(List<?>content,Integer page, Integer total) {
		this.total = total;
		this.content = content;
		this.page = page;
	}
	
	
	public Page(List<?>content,Integer page, Integer total, List<Facet> facets) {
		this.total = total;
		this.content = content;
		this.page = page;
		this.facets = facets;
	}
}