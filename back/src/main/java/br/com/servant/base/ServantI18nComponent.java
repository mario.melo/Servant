package br.com.servant.base;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class ServantI18nComponent implements Serializable
{
	private static final long serialVersionUID = 1L;
	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private MessageSource mensageSource;

	public String getProperty(String chave, String... params)
	{
		try
		{
			return this.mensageSource.getMessage(chave, params, LocaleContextHolder.getLocale());
		}
		catch (NoSuchMessageException e)
		{
			this.logger.info(e);
			this.logger.error("Key " + chave + " nao encontrada nos arquivos de propriedade da aplicacao");
		}
		return chave;
	}

	public String getProperty(ServantI18N comumI18n, String... params)
	{
		try
		{
			return this.mensageSource.getMessage(comumI18n.chave(), params, LocaleContextHolder.getLocale());
		}
		catch (NoSuchMessageException e)
		{
			this.logger.info(e);
			this.logger.error("Key " + comumI18n.chave() + " nao encontrada nos arquivos de propriedade da aplicacao");
		}
		return comumI18n.chave();
	}
}
