package br.com.servant.base;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ServantAppicationProvider  implements ApplicationContextAware, Serializable
{
	private static final long serialVersionUID = 1L;
	private final Log logger = LogFactory.getLog(getClass());
	private static ApplicationContext applicationContext = null;
	
	@Autowired
	private ApplicationContext context;
	

	@Override
	public void setApplicationContext(ApplicationContext context)
			throws BeansException
	{
		this.logger.debug("Setando o contexto manualmente");
		applicationContext = context;
	}

	public static ApplicationContext getApplicationContext()
	{
		return applicationContext;
	}

	public <T> T getBean(Class<T> bean)
	{
		if (getApplicationContext() != null) {
			try
			{
				return (T)applicationContext.getBean(bean);
			}
			catch (BeansException e)
			{
				this.logger.error("Bean inexistente: " + bean.toString(), e);
				return null;
			}
		}
		return null;
	}

	public Object getBean(String classPath)
	{
		if (getApplicationContext() != null) {
			try
			{
				return applicationContext.getBean(classPath);
			}
			catch (BeansException e)
			{
				this.logger.error("Bean inexistente: " + classPath, e);
				return null;
			}
		}
		return null;
	}
}