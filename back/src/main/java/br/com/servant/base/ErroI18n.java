package br.com.servant.base;

import java.io.Serializable;

public class ErroI18n implements Serializable
{
	private static final long serialVersionUID = 1L;
	private ServantI18N key;
	private String[] params;

	public ErroI18n() {}

	public ErroI18n(ServantI18N key, String... params)
	{
		this.key = key;
		this.params = params;
	}

	public ServantI18N getKey()
	{
		return this.key;
	}

	public void setKey(ServantI18N key)
	{
		this.key = key;
	}

	public String[] getParams()
	{
		return this.params;
	}

	public void setParams(String[] params)
	{
		this.params = params;
	}
}
