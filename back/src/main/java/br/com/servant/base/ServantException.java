package br.com.servant.base;

public class ServantException  extends RuntimeException
{
	private static final long serialVersionUID = 9104109396993245062L;
	private String codigo;

	public ServantException() {}

	public ServantException(String message)
	{
		super(message);
	}

	public ServantException(String codigo, String message)
	{
		super(message);
		this.codigo = codigo;
	}

	public ServantException(String codigo, String message, Throwable cause)
	{
		super(message, cause);
		this.codigo = codigo;
	}

	public ServantException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ServantException(Throwable cause)
	{
		super(cause);
	}

	public String getCodigo()
	{
		return this.codigo;
	}

	public void setCodigo(String codigo)
	{
		this.codigo = codigo;
	}

}
