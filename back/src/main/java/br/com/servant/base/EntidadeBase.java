package br.com.servant.base;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EntidadeBase implements Serializable
{
	private static final long serialVersionUID = 7086808939746169469L;

	public abstract Serializable getId();
}