package br.com.servant.apresentacao.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.servant.base.ErroI18n;
import br.com.servant.base.ServantAppicationProvider;
import br.com.servant.base.ServantI18N;
import br.com.servant.base.ServantI18nComponent;

public class Resposta implements Serializable
	 {
	   private static final long serialVersionUID = 6308812837366139389L;
	   ServantAppicationProvider wired = new ServantAppicationProvider();
	   ServantI18nComponent msgUtil;
	   @JsonInclude(JsonInclude.Include.NON_NULL)
	   private List<Mensagem> mensagens = null;
	   @JsonInclude(JsonInclude.Include.NON_NULL)
	   private Integer code;
	   @JsonInclude(JsonInclude.Include.NON_NULL)
	   private Object resultado;
	   
	   public Integer getCode()
	   {
	     return this.code;
	   }
	   
	   public List<Mensagem> getMensagens()
	   {
	     return this.mensagens;
	   }
	   
	   public Object getResultado()
	   {
	     return this.resultado;
	   }
	   
	   private Resposta(Builder builder)
	   {
	     this.code = builder.code;
	     this.mensagens = builder.mensagens;
	     this.resultado = builder.resultado;
	   }
	   
	   public static class Builder
	   {
		 ServantAppicationProvider wired = new ServantAppicationProvider();
		 ServantI18nComponent msgUtil;
	     private List<Mensagem> mensagens = null;
	     private Integer code;
	     private Object resultado;
	     
	     public Resposta build()
	     {
	       return new Resposta(this);
	     }
	     
	     public Builder code(Integer code)
	     {
	       this.code = code;
	       return this;
	     }
	     
	     public Builder mensagem(String mensagem, String... params)
	     {
	       if (this.mensagens == null) {
	         this.mensagens = new ArrayList();
	       }
	       Mensagem mensagemConvertida = converterMensagem(mensagem, params);
	       
	       this.mensagens.add(mensagemConvertida);
	       return this;
	     }
	     
	     public Builder mensagem(ServantI18N mensagem, String... params)
	     {
	       if (this.mensagens == null) {
	         this.mensagens = new ArrayList();
	       }
	       Mensagem mensagemConvertida = converterMensagem(mensagem, params);
	       
	       this.mensagens.add(mensagemConvertida);
	       return this;
	     }
	     
	     public Builder mensagens(Map<ServantI18N, String[]> mensagens)
	     {
	       if (this.mensagens == null) {
	         this.mensagens = new ArrayList();
	       }
	       List<Mensagem> mensagensConvertidas = converterMensagens(mensagens);
	       
	       this.mensagens.addAll(mensagensConvertidas);
	       return this;
	     }
	     
	     public Builder erros(List<ErroI18n> erros)
	     {
	       if (this.mensagens == null) {
	         this.mensagens = new ArrayList();
	       }
	       List<Mensagem> mensagensConvertidas = converterMensagens(erros);
	       
	       this.mensagens.addAll(mensagensConvertidas);
	       return this;
	     }
	     
	     private List<Mensagem> converterMensagens(List<ErroI18n> erros)
	     {
	       List<Mensagem> retorno = new ArrayList();
	       for (ErroI18n erro : erros) {
	         retorno.add(converterMensagem(erro.getKey(), erro.getParams()));
	       }
	       return retorno;
	     }
	     
	     private List<Mensagem> converterMensagens(Map<ServantI18N, String[]> mensagens)
	     {
	       List<Mensagem> retorno = new ArrayList();
	       
	       Set<Map.Entry<ServantI18N, String[]>> msgSet = mensagens.entrySet();
	       for (Map.Entry<ServantI18N, String[]> entry : msgSet) {
	         retorno.add(converterMensagem(((ServantI18N)entry.getKey()).chave(), (String[])entry.getValue()));
	       }
	       return retorno;
	     }
	     
	     private Mensagem converterMensagem(ServantI18N key, String... params)
	     {
	       this.msgUtil = ((ServantI18nComponent)this.wired.getBean(ServantI18nComponent.class));
	       
	       Mensagem msg = new Mensagem();
	       msg.setCodigo(key.chave());
	       msg.setTexto(this.msgUtil.getProperty(key.chave(), params));
	       
	       return msg;
	     }
	     
	     private Mensagem converterMensagem(String key, String... params)
	     {
	       this.msgUtil = ((ServantI18nComponent)this.wired.getBean(ServantI18nComponent.class));
	       
	       Mensagem msg = new Mensagem();
	       msg.setCodigo(key);
	       msg.setTexto(this.msgUtil.getProperty(key, params));
	       
	       return msg;
	     }
	     
	     public Builder resultado(Object objeto)
	     {
	       Assert.isNull(this.resultado, "O resultado j�� foi preenchido com objeto: " + this.resultado);
	       
	       this.resultado = objeto;
	       return this;
	     }
	   }
	 }