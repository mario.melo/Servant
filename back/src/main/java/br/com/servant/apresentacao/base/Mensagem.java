package br.com.servant.apresentacao.base;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Mensagem implements Serializable
{
	  private static final long serialVersionUID = 6696880562266032739L;
	  @JsonInclude(JsonInclude.Include.NON_NULL)
	  private String texto;
	  @JsonInclude(JsonInclude.Include.NON_NULL)
	  private String codigo;
	  @JsonInclude(JsonInclude.Include.NON_NULL)
	  private String propriedade;
	  
	  public String getTexto()
	  {
	    return this.texto;
	  }
	  
	  public void setTexto(String texto)
	  {
	    this.texto = texto;
	  }
	  
	  public String getCodigo()
	  {
	    return this.codigo;
	  }
	  
	  public void setCodigo(String codigo)
	  {
	    this.codigo = codigo;
	  }
	  
	  public String getPropriedade()
	  {
	    return this.propriedade;
	  }
	  
	  public void setPropriedade(String propriedade)
	  {
	    this.propriedade = propriedade;
	  }
	}
