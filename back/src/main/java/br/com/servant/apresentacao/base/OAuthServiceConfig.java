package br.com.servant.apresentacao.base;

import lombok.Getter;
import lombok.Setter;

import com.github.scribejava.core.builder.api.DefaultApi20;

public class OAuthServiceConfig {

	@Getter
	@Setter
	private String apiKey;
	@Getter
	@Setter
	private String apiSecret;
	@Getter
	@Setter
	private String callback;
	@Getter
	@Setter
	private DefaultApi20 api;
	@Getter
	@Setter
	private String scope;

	public OAuthServiceConfig() {
	}

	public OAuthServiceConfig(String apiKey, String apiSecret, String callback,
			DefaultApi20 api) {
		super();
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
		this.callback = callback;
		this.api = api;
	}
	
	public OAuthServiceConfig(String apiKey, String apiSecret, String callback, String scope,
			DefaultApi20 api) {
		super();
		this.scope = scope;
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
		this.callback = callback;
		this.api = api;
	}

}
