package br.com.servant.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.servant.base.EntidadeBase;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table
public class Telefone extends EntidadeBase{

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Id
	@Column(columnDefinition = "serial")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Getter
	@Setter
	@Column
	@NotNull
	private String ddd;

	@Getter
	@Setter
	@Column
	@NotNull
	private String telefone;

	@Transient
	public String getTelefoneFormatado() {
		return this.ddd + telefone;
	}

}
