package br.com.servant.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.servant.base.EntidadeBase;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Builder
@Entity
@AllArgsConstructor
@Table
public class Servico extends EntidadeBase{

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Id
	@Column(columnDefinition = "serial")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Getter
	@Setter
	@NotNull
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataSolicitacao;

	@Getter
	@Setter
	@Column
	private Integer nota;
	
	@Getter
	@Setter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_anuncio", insertable=false, updatable=false)
	private Anuncio anuncio;
	
	@Getter
	@Setter
	@NotNull
	@Column(name="id_anuncio")
	private Long anuncioId;
	
	@Getter
	@Setter
	@NotNull
	@Column(name="id_usuario")
	private Long usuarioId;
	
	@Size(max=500)
	@Column(name="mensagem_cliente")
	@Getter
	@Setter
	private String mensagemCliente;
	
	@Column(name="avaliacao_cliente",columnDefinition="text")
	@Getter
	@Setter
	private String avaliacaoCliente;
	
	@Getter
	@Setter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario", insertable=false, updatable=false)
	private Usuario usuario;
	
	@Getter
	@Setter
	@Column(name="servico_aceito_anunciante")
	private  Boolean servicoAceitoAnunciante;
	
	@Getter
	@Setter
	@Column(name="dias_estimados_realizar_servico")
	private Integer diasEstimadosRealizacaoServico;
} 