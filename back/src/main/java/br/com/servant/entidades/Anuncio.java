package br.com.servant.entidades;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.apache.lucene.analysis.br.BrazilianAnalyzer;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Latitude;
import org.hibernate.search.annotations.Longitude;
import org.hibernate.search.annotations.Resolution;
import org.hibernate.search.annotations.Spatial;
import org.hibernate.search.annotations.Store;

import br.com.servant.base.EntidadeBase;
import lombok.Getter;
import lombok.Setter;

@Spatial
@Indexed
@Entity
@Table
@Analyzer(impl=BrazilianAnalyzer.class)
public class Anuncio extends EntidadeBase{


	private static final long serialVersionUID = 1L;

	public static final String TITULO = "titulo"; 
	public static final String DESCRICAO = "descricao";
	public static final String DATA_CADASTRO = "dataCadastro";
	public static final String ID = "id";
	public static final String OCUPACAO_DESCRICAO = "ocupacao.descricao";
	public static final String UF = "endereco.cidade.uf";
	public static final String OCUPACAO_ID = "ocupacao.ocupacaoId";

	@Getter
	@Setter
	@Id
	@Column(columnDefinition = "serial")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Getter
	@Setter
	@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@NotNull
	@Column(columnDefinition="text")
	private String descricao;

	@Getter
	@Setter
	@Column(columnDefinition="int8 default 0")
	private Long visualizacoes = 0l;

	@Getter
	@Setter
	@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@NotNull
	@Column
	private String titulo;

	@Getter
	@Setter
	@Column
	@Latitude
	private Double latitude;

	@Getter
	@Setter
	@Column
	@Longitude
	private Double longitude;

	@Getter
	@Setter
	@Field(index=Index.YES, analyze=Analyze.NO, store=Store.YES)
	@DateBridge(resolution=Resolution.SECOND)
	@NotNull
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	@Getter
	@Setter
	@IndexedEmbedded
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_ocupacao")
	private Ocupacao ocupacao;
	
	@Getter
	@Setter
	@Column(name="id_ocupacao", insertable = false, updatable = false)
	private Long ocupacaoId;
	
	@Getter
	@Setter
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Setter
	@Getter
	@ManyToOne
	@IndexedEmbedded
	@JoinColumn(name="id_endereco")
	private Endereco endereco;

	@Getter
	@Setter
	@OneToMany(mappedBy="anuncio")
	private List<Arquivo> arquivos;

	@Getter
	@Setter
	@Field(analyze=Analyze.NO)
	@NotNull
	@Column
	private Boolean ativo;
	
	@Getter
	@Setter
	@Column
	private String email;
	
	@Getter
	@Setter
	@Column
	@NotNull
	private String telefone;
} 