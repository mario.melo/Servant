package br.com.servant.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.bridge.builtin.StringBridge;

import br.com.servant.base.EntidadeBase;
import br.com.servant.persistencia.util.UfBridge;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table
public class Cidade extends EntidadeBase{

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Id
	@Column
	private Long id;

	@Getter
	@Setter
	@NotNull
	@Column
	@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	private String nome;
	
	@Getter
	@Setter
	@Column(unique = true)
	private Integer ibge;
	
	@Getter
	@Setter
	@ManyToOne
	@FieldBridge(impl = UfBridge.class)
	@Field(index=Index.YES, analyze=Analyze.NO, store=Store.NO)
	private UF uf;

} 