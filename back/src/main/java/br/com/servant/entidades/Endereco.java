package br.com.servant.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;

import br.com.servant.base.EntidadeBase;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table
public class Endereco extends EntidadeBase{

	private static final long serialVersionUID = 1L;


	@Getter
	@Setter
	@Id
	@Column(columnDefinition = "serial")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Getter
	@Setter
	@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@NotNull
	@Column(length = 40)
	private String endereco;
	
	@Getter
	@Setter
	@Column(length = 25)
	@Field(index=Index.YES, analyze=Analyze.NO, store=Store.NO)
	private String bairro;
	
	@Getter
	@Setter
	@Column(length = 8)
	private String cep;
	
	@Getter
	@Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn
	@IndexedEmbedded
	private Cidade cidade;
} 