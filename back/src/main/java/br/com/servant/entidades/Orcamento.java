package br.com.servant.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import br.com.servant.base.EntidadeBase;

@NoArgsConstructor
@Builder
@Entity
@AllArgsConstructor
@Table
public class Orcamento extends EntidadeBase{

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Id
	@Column(columnDefinition = "serial")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Getter
	@Setter
	@NotNull
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataSolicitacao;

	@Getter
	@Setter
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataResposta;

	@Getter
	@Setter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_anuncio", insertable=false, updatable=false)
	private Anuncio anuncio;
	
	@Getter
	@Setter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_endereco", insertable=false, updatable=false)
	private Endereco endereco;
	
	@Getter
	@Setter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_ocupacao", insertable=false, updatable=false)
	private Ocupacao ocupacao;
	
	@Setter
	@Getter
	@Column(name = "id_ocupacao")
	private Long ocupacaoId;
	
	@Setter
	@Getter
	@Column(name = "id_cidade")
	private Long municipioId;

	@Getter
	@Setter
	@Column(name="id_anuncio")
	private Long anuncioId;

	@Getter
	@Setter
	@NotNull
	@Column(name="id_usuario")
	private Long usuarioId;

	@NotNull
	@Column(columnDefinition="text")
	@Getter
	@Setter
	private String pedido;

	@Column(columnDefinition="text", name="resposta_anunciante")
	@Getter
	@Setter
	private String respostaAnunciante;

	@Getter
	@Setter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario", insertable=false, updatable=false)
	private Usuario usuario;

	@Getter
	@Setter
	@Column(name="aceita_pedido_orcamento")
	private Boolean aceitaPedido;

	@Getter
	@Setter
	@Column(name="resposta_visualizada")
	private Boolean respostaVisualizada;
} 