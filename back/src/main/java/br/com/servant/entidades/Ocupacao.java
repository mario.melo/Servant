package br.com.servant.entidades;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Facet;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.bridge.builtin.LongBridge;

import br.com.servant.base.EntidadeBase;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table
public class Ocupacao extends EntidadeBase {

	private static final long serialVersionUID = 1L;

	@Id
	@Getter
	@Setter
	@Field(name = "ocupacaoId",analyze = Analyze.NO, store = Store.YES, index = Index.YES)
	@FieldBridge(impl = LongBridge.class)
	private Long id;

	@Getter
	@Setter
	@Facet
	@Field(analyze= Analyze.NO)
	@NotNull
	@Column
	private String descricao;

	@Getter
	@Setter
	@IndexedEmbedded(depth=1)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name ="id_ocupacao_pai")
	private Ocupacao ocupacao;

	@Getter
	@Setter
	@Column(name ="id_ocupacao_pai", insertable=false, updatable=false)
	private Long idOcupacaoPai;

	@Getter
	@Setter
	@Column(name="ordem_exibicao")
	private Integer ordemExibicao;
	
	@Getter
	@Setter
	@Column(nullable = true)
	private String icone;

	@Getter
	@Setter
	@OneToMany(mappedBy = "ocupacao", fetch=FetchType.LAZY)
	private List<Ocupacao> ocupacoes;
}