package br.com.servant.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import br.com.servant.base.EntidadeBase;

@Entity
@Table(name="MENSAGEM")
public class Mensagem extends EntidadeBase{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition = "serial")
	@Getter
	@Setter
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Getter
	@Setter
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataMensagem;
	
	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name="id_anuncio")
	private Anuncio anuncio;
	
	@Column(columnDefinition="text")
	@Getter
	@Setter
	private String menssagem;
	
	@Getter
	@Setter
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@Getter
	@Setter
	@Column(columnDefinition="bool default FALSE")
	private Boolean visualizada;


}
