package br.com.servant.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.servant.base.EntidadeBase;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UF extends EntidadeBase{
	
	private static final long serialVersionUID = -3968900683586737428L;
	@Getter
	@Setter
	@Id
	@Column(name = "sigla")
	private String id;
	
}