package br.com.servant.entidades;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.servant.base.EntidadeBase;
import br.com.servant.dominio.SexoEnum;
import br.com.servant.dominio.TipoUsuarioEnum;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table
public class Usuario extends EntidadeBase{

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Id
	@Column(columnDefinition = "serial")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Getter
	@Setter
	@Column
	private String socialId;

	@Getter
	@Setter
	@NotNull
	@Column
	private String nome;

	@Getter
	@Setter
	@Column
	private String email;

	@Getter
	@Setter
	@Column
	@Enumerated(EnumType.STRING)
	private SexoEnum sexo;

	@Getter
	@Setter
	@NotNull
	@Column
	@Enumerated(EnumType.STRING)
	private TipoUsuarioEnum tipoUsuarioEnum;

	@Getter
	@Setter
	@Column
	private String login;

	@Getter
	@Setter
	@Column
	private String senha;

	@Getter
	@Setter
	@OneToMany(mappedBy="usuario", fetch=FetchType.LAZY)
	private List<Anuncio> anuncios;

	@Getter
	@Setter
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name ="id_telefone")
	private Telefone telefone;

}