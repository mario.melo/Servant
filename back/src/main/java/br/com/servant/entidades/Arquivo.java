package br.com.servant.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import br.com.servant.base.EntidadeBase;

@Entity
@Table(name="ARQUIVO")
public class Arquivo extends EntidadeBase{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(columnDefinition = "serial")
	@Getter
	@Setter
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Getter
	@Setter
	@Column
	private String nome;

//	@Basic(fetch=FetchType.LAZY)
//	@Lob
//	@Column(name="FILE")
//	private Long[] file;
	
	@Getter
	@Setter
	@Column
	private String diretorio;
	
	@Getter
	@Setter
	@Column(columnDefinition="bool default FALSE")
	private Boolean imagemPrincipal;
	
	@Getter
	@Setter
	@Column(columnDefinition="text")
	private String base64;
	
	@Getter
	@Setter
	@Column(columnDefinition="text")
	private String thumbnail;
	
	@Getter
	@Setter
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	
	@Getter
	@Setter
	@ManyToOne
	@JoinColumn
	private Anuncio anuncio;

	public Arquivo(String diretorio, Anuncio anuncio, Date dataCadastro, String base64, String thumbnail) {
		super();
		this.dataCadastro = dataCadastro;
		this.anuncio = anuncio;
		this.diretorio = diretorio;
		this.base64 = base64;
		this.thumbnail = thumbnail;
	}

	public Arquivo() {
		
	}
}
