package br.com.servant.configuration;

import lombok.Getter;
import lombok.Setter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.oauth2.OAuth2Parameters;

import br.com.servant.apresentacao.base.OAuthServiceConfig;

import com.github.scribejava.apis.FacebookApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.oauth.OAuthService;

@Configuration
public class SocialConfiguration {

	@Value("${app.config.oauth.facebook.apikey}")
	private String facebookApiKey;
	@Value("${app.config.oauth.facebook.apisecret}")
	private String facebookApiSecret;
	@Value("${app.config.oauth.facebook.callback}")
	private String facebookRdirectUri;
	@Value("${app.config.oauth.facebook.scope}")
	private String facebookScope;

	@Value("${app.config.oauth.google.apikey}")
	private String googleApiKey;
	@Value("${app.config.oauth.google.apisecret}")
	private String googleApiSecret;


	@Bean
	public FacebookConnectionFactory facebookConnectionFactory(){
		return new FacebookConnectionFactory(facebookApiKey, facebookApiSecret);
	}
	@Bean
	public GoogleConnectionFactory googleConnectionFactory(){
		return new GoogleConnectionFactory(googleApiKey, googleApiSecret);
	}

	@Bean
	public SocialService servantFacebookSocialService(){


		OAuth2Parameters oAuth2Parameters = new OAuth2Parameters();
		oAuth2Parameters.setScope(facebookScope);
		oAuth2Parameters.setRedirectUri(facebookRdirectUri);

		return new SocialService(oAuth2Parameters,
				new OAuthServiceConfig(facebookApiKey, facebookApiSecret, facebookRdirectUri, facebookScope, FacebookApi.instance()));
	}


	public class SocialService{


		@Getter
		@Setter
		private OAuth2Parameters oAuth2Parameters;

		@Getter
		@Setter
		private OAuthServiceConfig oAuthServiceConfig;

		public OAuthService<?> getOAuthService() {
			return new ServiceBuilder()
			.apiKey(oAuthServiceConfig.getApiKey())
			.apiSecret(oAuthServiceConfig.getApiSecret())
			.callback(oAuthServiceConfig.getCallback())
			.build(oAuthServiceConfig.getApi());
		}


		public SocialService(OAuth2Parameters oAuth2Parameters, OAuthServiceConfig oAuthServiceConfig){
			this.oAuth2Parameters = oAuth2Parameters;
			this.oAuthServiceConfig = oAuthServiceConfig;
		}

	} 
}