package br.com.servant.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import br.com.servant.negocio.seguranca.AjaxLogoutSuccessHandler;
import br.com.servant.negocio.seguranca.ServantHttp403ForbiddenEntryPoint;

@Configuration
@EnableAuthorizationServer
public class OAuthConfiguration extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private RedisTokenStore redisTokenStore;

	//@Autowired
	//private UserDetailsService userDetailsService;

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints
		//.userDetailsService(userDetailsService)
		.tokenStore(redisTokenStore)
		.authenticationManager(authenticationManager)
		.reuseRefreshTokens(true);
	}
	


    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.allowFormAuthenticationForClients();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients
                .inMemory()
                .withClient("servant")
                .scopes("read", "write")
                .authorities("ROLE_ADMIN", "ROLE_USER")
                .authorizedGrantTypes("password", "refresh_token", "authorization_code", "implicit")
                .secret("servant")
                .accessTokenValiditySeconds(7500)
                .refreshTokenValiditySeconds(30000);
    }


	@Configuration
	@EnableResourceServer
	protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {


		@Autowired
		private ServantHttp403ForbiddenEntryPoint servantHttp403ForbiddenEntryPoint;
		
		@Autowired
		private AjaxLogoutSuccessHandler ajaxLogoutSuccessHandler;


		@Override
		public void configure(HttpSecurity http) throws Exception {

			http.exceptionHandling()
			.authenticationEntryPoint(servantHttp403ForbiddenEntryPoint)
			.and()
			.logout()
			.logoutUrl("/api/private/login/logout")
			.logoutSuccessHandler(ajaxLogoutSuccessHandler)
			.and()
			.csrf()
			.disable()
			.headers()
			.frameOptions().disable()
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			
			ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry = http.authorizeRequests();
			registry
			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
			
			.antMatchers(HttpMethod.GET, "/api/private/**").authenticated()
            .antMatchers(HttpMethod.POST, "/api/private/**").authenticated()
            .antMatchers(HttpMethod.PUT, "/api/private/**").authenticated()
            .antMatchers(HttpMethod.DELETE, "/api/private/**").authenticated()
			
			.antMatchers(HttpMethod.GET, "/api/**").permitAll()
            .antMatchers(HttpMethod.POST, "/api/**").permitAll()
            .antMatchers(HttpMethod.PUT, "/api/**").permitAll()
            .antMatchers(HttpMethod.DELETE, "/api/**").permitAll()
			.antMatchers(HttpMethod.GET, "/api/public/**").permitAll()
            .antMatchers(HttpMethod.POST, "/api/public/**").permitAll()
            .antMatchers(HttpMethod.PUT, "/api/public/**").permitAll()
            .antMatchers(HttpMethod.DELETE, "/api/public/**").permitAll();
            
			
		}
    }

}
