package br.com.servant.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.servant.base.ServantException;
import br.com.servant.base.ServantI18nComponent;

@ControllerAdvice
public class ServantExceptionHandler extends ResponseEntityExceptionHandler {
	
	
	@Autowired
	private ServantI18nComponent comum18NUtil;

	@ExceptionHandler(value = ServantException.class)
	 @ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ResponseEntity<Object> exception(ServantException exception) {
		
		
		System.out.println("TESTEs");
		
		return null;
		
	}

}
