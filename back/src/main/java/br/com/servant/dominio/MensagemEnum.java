package br.com.servant.dominio;

import br.com.servant.base.ServantI18N;

public enum MensagemEnum implements ServantI18N {
	

	OPERACAO_NAO_PERMITIDA("operacao.nao.permitida"),
		
	MSG_ERRO_DESCONHECIDO("erro.desconhecido"), 
	
	USUARIO_NAO_AUTORIZADO("usuario.nao.autorizado"),
	
	
	
	NULL;
	

	/** The valor. */
	private String valor;

	/**
	 * Instantiates a new mensagem enum.
	 *
	 * @param valor the valor
	 */
	private MensagemEnum(String valor) {
		this.valor = valor;
	}
	
	/**
	 * Instantiates a new mensagem enum.
	 */
	private MensagemEnum() {
	}

	/**
	 * Valor.
	 *
	 * @return the string
	 */
	public String valor() {
		return this.valor;
	}

	/**
	 * Value of arg.
	 *
	 * @param message the message
	 * @return the mensagem enum
	 */
	public static MensagemEnum valueOfArg(String message) {
		for (MensagemEnum msg : values()) {
			if (msg.valor().equals(message)) {
				return msg;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see br.com.dwlti.base.comum.i18n.ComumI18N#chave()
	 */
	@Override
	public String chave() {
		if(this.valor != null){
			return this.valor;
		}else{
			return this.name();
		}
	}

}
