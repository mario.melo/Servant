package br.com.servant.dominio;

import java.io.Serializable;


public enum SexoEnum implements Serializable {
	
	MASCULINO,
	FEMININO;

}