package br.com.servant.negocio.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.google.api.plus.Person;
import org.springframework.stereotype.Service;

import br.com.servant.dominio.SexoEnum;
import br.com.servant.dominio.TipoUsuarioEnum;
import br.com.servant.entidades.Anuncio;
import br.com.servant.entidades.Usuario;
import br.com.servant.negocio.seguranca.OAuthProvider;
import br.com.servant.negocio.service.UsuarioService;
import br.com.servant.persistencia.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository repository;

	@Autowired
	private OAuthProvider comumProvider;
	
	@Override
	public Usuario getUsuario(Person person) {
		Usuario user = repository.findBySocialIdAndTipoUsuarioEnum(person.getId(), TipoUsuarioEnum.GOOGLE);

		if(user == null){
			user = createUsuario(person);
			repository.save(user);
		}
		return user;
	}

	@Override
	public Usuario salvar(Usuario u) {
		return repository.save(u);
	}

	private Usuario createUsuario(Person p){

		Usuario u = new Usuario();

		u.setNome(p.getDisplayName());
		u.setSocialId(p.getId());
		u.setTipoUsuarioEnum(TipoUsuarioEnum.GOOGLE);
		u.setSexo(SexoEnum.MASCULINO);
		u.setSenha("socialPassGoogle");
		u.setEmail(p.getEmails() != null && !p.getEmails().isEmpty() ? p.getEmails().get(0) : null );
		return u;
	}
	
	private Usuario createUsuario(FacebookProfile profile){

		Usuario u = new Usuario();

		u.setNome(profile.getName());
		u.setSocialId(profile.getId());
		u.setTipoUsuarioEnum(TipoUsuarioEnum.FACEBOOK);
		u.setSexo(SexoEnum.MASCULINO);
		u.setSenha("socialPassFacebook");
		return u;
	}

	@Override
	public Usuario getUsuario(FacebookProfile facebookProfile) {
		Usuario user = repository.findBySocialIdAndTipoUsuarioEnum(facebookProfile.getId(), TipoUsuarioEnum.FACEBOOK);

		if(user == null){
			user = createUsuario(facebookProfile);
			repository.save(user);
		}
		return user;
	}

	@Override
	public void createAnuncio(Anuncio anuncio) {
		
		Usuario u = comumProvider.getUsuarioLogado();
		
		if(u.getAnuncios() == null){
			u.setAnuncios(new ArrayList<Anuncio>());
		}

		u.getAnuncios().add(anuncio);
	}
}