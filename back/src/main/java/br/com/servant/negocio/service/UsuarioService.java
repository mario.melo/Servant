package br.com.servant.negocio.service;

import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.google.api.plus.Person;

import br.com.servant.entidades.Anuncio;
import br.com.servant.entidades.Usuario;

public interface UsuarioService {

	Usuario getUsuario(Person person);

	Usuario salvar(Usuario u);

	Usuario getUsuario(FacebookProfile facebookProfile);

	void createAnuncio(Anuncio anuncio);

}
