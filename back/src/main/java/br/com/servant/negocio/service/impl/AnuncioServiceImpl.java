package br.com.servant.negocio.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import br.com.servant.base.ServantException;
import br.com.servant.base.ServantI18nComponent;
import br.com.servant.dominio.MensagemEnum;
import br.com.servant.dtos.DetalheAnuncioDTO;
import br.com.servant.dtos.MeusAnunciosDTO;
import br.com.servant.dtos.NegociosPendentesDTO;
import br.com.servant.dtos.OrcamentoDTO;
import br.com.servant.entidades.Anuncio;
import br.com.servant.entidades.Arquivo;
import br.com.servant.entidades.Orcamento;
import br.com.servant.entidades.Servico;
import br.com.servant.entidades.Usuario;
import br.com.servant.negocio.seguranca.OAuthProvider;
import br.com.servant.negocio.service.AnuncioService;
import br.com.servant.persistencia.repository.AnuncioRepository;
import br.com.servant.persistencia.repository.ArquivoRepository;
import br.com.servant.persistencia.repository.EnderecoRepository;
import br.com.servant.persistencia.repository.OrcamentoRepository;
import br.com.servant.persistencia.repository.ServicoRepository;
import br.com.servant.util.ArquivoUtil;

@Service
public class AnuncioServiceImpl implements AnuncioService {
 
	@Autowired
	private AnuncioRepository repository;

	@Autowired
	private OAuthProvider oauthProvider;

	@Autowired
	private ArquivoUtil arquivoUtil;

	@Autowired
	private ArquivoRepository arquivoRepository;
	
	@Autowired
	private EnderecoRepository enderecoRepository;

	@Autowired
	private ServicoRepository servicoRepository;

	@Autowired
	private ServantI18nComponent comum18NUtil;

	@Autowired
	private OrcamentoRepository orcamentoRepository;

	@Transactional
	@Override
	public void salvar(Anuncio anuncio, List<MultipartFile> files) {

		Usuario usuario = oauthProvider.getUsuarioLogado();

		anuncio.setDataCadastro(new Date());
		anuncio.setUsuario(usuario);
		anuncio.setAtivo(true);
		
		anuncio.getEndereco().setCep(anuncio.getEndereco().getCep().replaceAll("\\D+",""));
		
		enderecoRepository.save(anuncio.getEndereco());
		repository.save(anuncio);

		if(files != null){
			List<Arquivo> arquivos = arquivoUtil.salvarArquivosFisico(usuario.getId(), files, anuncio);
			arquivos.get(0).setImagemPrincipal(true);
			for(int i = 0 ; i < arquivos.size(); i++){
				arquivoRepository.save(arquivos.get(i));
			}
		}
	}
	
	@Override
	public List<MeusAnunciosDTO> getAnunciosUsuarioLogado() {
		return repository.getAnunciosUsuarioLogado(oauthProvider.getUsuarioTokenLogado().getIdUsuarioAutenticado());
	}

	@Override
	public DetalheAnuncioDTO getAnuncio(Long id) {
		DetalheAnuncioDTO result = repository.getDetalheAnuncio(id);
		result.setImagens(repository.getImagensAnuncio(id));
		return result;
	}

	@Transactional
	@Override
	public void visualizado(Long id) {
		repository.visualizarAnuncio(id);
	}

	@Override
	public void solicitarNegocio(Long id) {

		validarUsuarioAnuncio(id);

		Servico servico = Servico.builder()
				.anuncioId(id)
				.dataSolicitacao(new Date())
				.usuarioId(oauthProvider.getIdUsuarioLogado())
				.build();

		servicoRepository.save(servico);
	}

	@Override
	public NegociosPendentesDTO negociosPendentes() {
		return repository.getNegociosPendentes(oauthProvider.getIdUsuarioLogado());
	}

	@Override
	public List<NegociosPendentesDTO> getDetalheServicosPendentes() {
		return repository.getDetalheSolicitacaoServico(oauthProvider.getIdUsuarioLogado());
	}

	@Override
	public void solicitarOrcamento(Orcamento orcamento) {

		validarUsuarioAnuncio(orcamento.getAnuncioId());

		orcamento.setDataSolicitacao(new Date());
		orcamento.setUsuarioId(oauthProvider.getIdUsuarioLogado());
		
		orcamento.getEndereco().setCep(orcamento.getEndereco().getCep().replaceAll("\\D+",""));
		enderecoRepository.save(orcamento.getEndereco());
		
		orcamentoRepository.save(orcamento);
	}

	private void validarUsuarioAnuncio(Long id){
		
		if(id == null) {
			return;
		}
		if(repository.validarIdUsuario(oauthProvider.getIdUsuarioLogado(), id)){
			throw new ServantException(comum18NUtil.getProperty(MensagemEnum.OPERACAO_NAO_PERMITIDA));
		}
	}

	@Override
	public void avaliarPedidoNegocio(NegociosPendentesDTO negocio) {
		Servico servico =  servicoRepository.findOne(negocio.getId());

		if(servico.getUsuarioId().equals(oauthProvider.getIdUsuarioLogado())){
			throw new ServantException(comum18NUtil.getProperty(MensagemEnum.OPERACAO_NAO_PERMITIDA));
		}
		servico.setServicoAceitoAnunciante(negocio.getAceita());

		servicoRepository.save(servico);
	}

	@Override
	public List<NegociosPendentesDTO> getDetalheOrcamentosPendentes() {
		return repository.getDetalheSolicitacaoOrcamento(oauthProvider.getIdUsuarioLogado());
	}

	@Override
	public void responderSolicitacaoOrcamento(NegociosPendentesDTO negocio) {

		Orcamento orcamento = orcamentoRepository.findOne(negocio.getId());

		orcamento.setAceitaPedido(negocio.getAceita());
		orcamento.setRespostaAnunciante(negocio.getResposta());
		orcamento.setDataResposta(new Date());

		orcamentoRepository.save(orcamento); 

	}

	@Override
	public Page<OrcamentoDTO> getRespostasSolicitacaoOrcamento(int page){
		
		PageRequest pageRequest = new PageRequest(page, 10);
		
		return repository.getRespostasSolicitacaoOrcamento(oauthProvider.getIdUsuarioLogado(), pageRequest);
	}
}