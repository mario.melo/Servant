package br.com.servant.negocio.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import br.com.servant.entidades.Cidade;
import br.com.servant.entidades.Ocupacao;
import br.com.servant.entidades.Orcamento;
import br.com.servant.entidades.UF;
import br.com.servant.negocio.seguranca.OAuthProvider;
import br.com.servant.negocio.service.OcupacaoService;
import br.com.servant.persistencia.repository.OcupacaoRepository;
import br.com.servant.persistencia.repository.OrcamentoRepository;

@Service
public class OcupacaoServiceImpl implements OcupacaoService {

	@Autowired
	private OcupacaoRepository repository;
	
	@Autowired
	private OrcamentoRepository orcamentoRepository;

	@Autowired
	private OAuthProvider oauthProvider;
	
	@Cacheable("ocupacoesPrincipais")
	@Override
	public List<Ocupacao> findOcupacoesFirstLevel() {
		return repository.findOcupacaoFirstLevel();
	}

	@Cacheable("subocupacoes")
	@Override
	public List<Ocupacao> findSubOcupacoes(Long id) {
		return repository.findSubOcupacoes(id);
	}
	
	@Cacheable("ufs")
	@Override
	public List<UF> ufs() {
		return repository.getUFs();
	}
	
	@Cacheable("municipios")
	@Override
	public List<Cidade> getMunicipios(String uf) {
		return repository.getMunicipios(uf);
	}
	
	@Override
	public Cidade getMunicipio(Integer ibge) {
		return repository.getMunicipio(ibge);
	}
	
	@Override
	public List<Orcamento> getOrcamentosAbertos() {
		return orcamentoRepository.getOrcamentosAbertos(oauthProvider.getIdUsuarioLogado());
	}

}