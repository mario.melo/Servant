package br.com.servant.negocio.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import br.com.servant.dtos.DetalheAnuncioDTO;
import br.com.servant.dtos.MeusAnunciosDTO;
import br.com.servant.dtos.NegociosPendentesDTO;
import br.com.servant.dtos.OrcamentoDTO;
import br.com.servant.entidades.Anuncio;
import br.com.servant.entidades.Orcamento;

public interface AnuncioService {

	void salvar(Anuncio anuncio, List<MultipartFile> files);
	
	List<MeusAnunciosDTO>  getAnunciosUsuarioLogado();

	DetalheAnuncioDTO getAnuncio(Long id);

	void visualizado(Long id);

	void solicitarNegocio(Long id);

	NegociosPendentesDTO negociosPendentes();

	List<NegociosPendentesDTO> getDetalheServicosPendentes();

	void avaliarPedidoNegocio(NegociosPendentesDTO negocio);

	void solicitarOrcamento(Orcamento orcamento);

	List<NegociosPendentesDTO> getDetalheOrcamentosPendentes();

	void responderSolicitacaoOrcamento(NegociosPendentesDTO negocio);

	Page<OrcamentoDTO> getRespostasSolicitacaoOrcamento(int page);
}
