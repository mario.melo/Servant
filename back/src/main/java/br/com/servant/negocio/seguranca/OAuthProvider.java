package br.com.servant.negocio.seguranca;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.servant.base.ServantException;
import br.com.servant.base.ServantI18nComponent;
import br.com.servant.dominio.MensagemEnum;
import br.com.servant.dominio.TipoUsuarioEnum;
import br.com.servant.entidades.Usuario;
import br.com.servant.persistencia.repository.UsuarioRepository;

@Component
public class OAuthProvider implements AuthenticationProvider  {

	private static final String ROLE_CLIENT_OAUTH = "ROLE_CLIENT_OAUTH";


	private static final Log LOGGER = LogFactory.getLog(OAuthProvider.class);

	@Autowired
	private ServantI18nComponent comum18NUtil;

	@Autowired
	private TokenStore redisTokenStore;

	@Autowired(required=true)
	private HttpServletRequest request;

	@Autowired
	private UsuarioRepository usuarioRepository;

	private static final String COOKIE_SERVANT = "servantToken";

	private UsuarioComumToken criarUsuario(Usuario user, String senha){

		UsuarioComumToken token = null;
		token = criarToken(user, senha);

		return token;
	}

	private UsuarioComumToken criarToken(Usuario user, String senha) {
		List<GrantedAuthority> autorizacoes = new ArrayList<GrantedAuthority>();
		autorizacoes.add(new SimpleGrantedAuthority(ROLE_CLIENT_OAUTH));
		autorizacoes.add(new SimpleGrantedAuthority(String.valueOf(new Date().getTime())));
		UsuarioComumToken token = new UsuarioComumToken(user.getId(), senha, autorizacoes);

		token.setEmail(user.getEmail());
		token.setNome(user.getNome());
		token.setSenha(senha);
		token.setUserName(user.getTipoUsuarioEnum() != TipoUsuarioEnum.SERVANT ? user.getSocialId() : user.getLogin());
		token.setIdUsuarioAutenticado(user.getId());
		token.setTipoAutenticacao(user.getTipoUsuarioEnum());
		token.setTelefone(user.getTelefone().getTelefoneFormatado());
		return token;
	}

	public UsuarioComumToken getUsuarioTokenLogado() {
		OAuth2Authentication auth = (OAuth2Authentication)SecurityContextHolder.getContext().getAuthentication();
		UsuarioComumToken usuario = null;

		if ( auth != null && auth.getUserAuthentication() != null ) {
			usuario = (UsuarioComumToken) auth.getUserAuthentication();
		}
		return usuario;
	}

	public Usuario getUsuarioLogado() {

		OAuth2Authentication auth = (OAuth2Authentication)SecurityContextHolder.getContext().getAuthentication();

		UsuarioComumToken user = (UsuarioComumToken)auth.getUserAuthentication();

		return usuarioRepository.findOne((Long)user.getPrincipal());
	}


	public Long getIdUsuarioLogado() {

		OAuth2Authentication auth = (OAuth2Authentication)SecurityContextHolder.getContext().getAuthentication();

		UsuarioComumToken user = (UsuarioComumToken)auth.getUserAuthentication();

		return (Long)user.getPrincipal();
	}

	public String getToken() {
		DefaultOAuth2AccessToken o = (DefaultOAuth2AccessToken)SecurityContextHolder.getContext().getAuthentication().getDetails();

		return o.getValue();
	}

	public DefaultOAuth2AccessToken getOAuth2AccessToken() {
		DefaultOAuth2AccessToken o = (DefaultOAuth2AccessToken)SecurityContextHolder.getContext().getAuthentication().getDetails();

		return o;
	}


	@Override
	@Transactional(readOnly = true)
	public Authentication authenticate(Authentication authetication) throws AuthenticationException {
		boolean isAutenticacao = isAutenticacao(authetication);

			if (isAutenticacao) {

				UsuarioComumToken user = null;


				String login = authetication.getName();

				Usuario usuario = usuarioRepository.findByLoginAndSenha(login, (String)authetication.getCredentials());

				if(usuario == null){
					throw new ServantException(comum18NUtil.getProperty(MensagemEnum.USUARIO_NAO_AUTORIZADO.valor()));
				}
				user = criarUsuario(usuario, (String)authetication.getCredentials());

				return user;
			}else{
				UsuarioComumToken token = (UsuarioComumToken)authetication.getPrincipal();
				return token;
			}

	}

	public void socialAuthenticate(Usuario usuario){
		////		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(, "socialPass");
		//
		//		ComumTokenServices tokenService = new ComumTokenServices();
		//
		//		HashMap<String, String> authorizationParameters = new HashMap<String, String>();
		//		authorizationParameters.put("scope", "read");
		//		authorizationParameters.put("username", "servant");
		//		authorizationParameters.put("client_id", "servant");
		//		authorizationParameters.put("grant", "password");
		//
		//		DefaultAuthorizationRequest authorizationRequest = new DefaultAuthorizationRequest(authorizationParameters);
		//		authorizationRequest.setApproved(true);
		//
		//		tokenService.setTokenStore(redisTokenStore);
		//
		//		OAuth2Authentication auth = new OAuth2Authentication(authorizationRequest, criarToken(usuario, "socialPass"));
		//
		//		OAuth2AccessToken accessToken = tokenService.createAccessToken(auth);
		////		token.setDetails(accessToken);
		//
		//		auth.setDetails(accessToken);
		//
		//		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	private boolean isAutenticacao(Authentication authetication) {

		if(authetication.getPrincipal() instanceof UsuarioComumToken){
			return false;
		}
		return true;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}