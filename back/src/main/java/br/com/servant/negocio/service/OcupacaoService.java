package br.com.servant.negocio.service;

import java.util.List;

import br.com.servant.entidades.Cidade;
import br.com.servant.entidades.Ocupacao;
import br.com.servant.entidades.Orcamento;
import br.com.servant.entidades.UF;

public interface OcupacaoService {

	List<Ocupacao> findOcupacoesFirstLevel();

	List<Ocupacao>  findSubOcupacoes(Long id);

	List<UF> ufs();

	List<Cidade> getMunicipios(String uf);

	Cidade getMunicipio(Integer ibge);

	List<Orcamento> getOrcamentosAbertos();

}
