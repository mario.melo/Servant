package br.com.servant.negocio.service.impl;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Service;

import br.com.servant.entidades.Anuncio;
import br.com.servant.negocio.service.SearchService;

@Service
public class SearchServiceImpl implements SearchService {


	@PersistenceContext
	private EntityManager em;
	
	
	private FullTextEntityManager fullTextEntityManager;
	

	@Override
	public void consultarAnucnio(String term) {
		
		Query query = getAnuncioQueryBuilder().keyword().onFields(Anuncio.DESCRICAO, Anuncio.TITULO)
						.matching(term).createQuery();
		
		
		
		javax.persistence.Query persistenceQuery = getFullTextEntityManager().createFullTextQuery(query);

	}


	private QueryBuilder getAnuncioQueryBuilder(){
		return getFullTextEntityManager().getSearchFactory().buildQueryBuilder().forEntity(Anuncio.class).get();
	}
	
	//TODO: isso deve virar um bean
	private FullTextEntityManager getFullTextEntityManager(){
		if(fullTextEntityManager == null){
			fullTextEntityManager = Search.getFullTextEntityManager(em);
		}
		return fullTextEntityManager;
	}

}