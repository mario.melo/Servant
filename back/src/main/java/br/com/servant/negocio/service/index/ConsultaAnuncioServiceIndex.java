package br.com.servant.negocio.service.index;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.BooleanJunction;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.search.query.dsl.Unit;
import org.hibernate.search.query.engine.spi.FacetManager;
import org.hibernate.search.query.facet.FacetSortOrder;
import org.hibernate.search.query.facet.FacetingRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.servant.dtos.AnuncioDTO;
import br.com.servant.entidades.Anuncio;
import br.com.servant.negocio.service.ConsultaAnuncioIndexService;
import br.com.servant.persistencia.repository.AnuncioRepository;
import br.com.servant.resource.controller.ConsultaController.Filter;
import br.com.servant.util.Page;

@Service
public class ConsultaAnuncioServiceIndex implements ConsultaAnuncioIndexService{

	private final String FACET_CATEGORIA = "facetCategoria";

	@PersistenceContext
	@Autowired
	private EntityManager entityManager;

	@Value("${servant.index.maxResult}")
	private Integer maxResult;

	@Autowired
	private AnuncioRepository anuncioRepository;

	@Transactional
	@Override
	public Page consultar(Filter filter) {
		FullTextEntityManager fullTextEntityManager = getFullTextEntityManager(entityManager);

		QueryBuilder qb = fullTextEntityManager.getSearchFactory()
				.buildQueryBuilder().forEntity(Anuncio.class).get();

		FacetManager facetManager = null;

		FullTextQuery fullTextQuery = null;

		Query query = criarQuery(filter, fullTextEntityManager, qb);

		if(query != null){
			fullTextQuery = criarFullTextQuery(filter, fullTextEntityManager, query);
			facetManager = fullTextQuery.getFacetManager();
			facetManager.enableFaceting(setFacetCategoria(qb));
		}else{
			query = criarQueryUltimosAnuncios(filter, fullTextEntityManager, qb);
			fullTextQuery =  criarFullTextQuery(filter, fullTextEntityManager, query);
		}

		Sort sort = qb
				.sort()
				.byField(Anuncio.DATA_CADASTRO).desc() 
				.createSort();
		fullTextQuery.setSort(sort);

		if(fullTextQuery.getResultSize() > 0){

			List<AnuncioDTO> results = realizarConsulta(fullTextQuery); 
			if(facetManager != null)
				return new Page(results, filter.getPage(), fullTextQuery.getResultSize(), facetManager.getFacets(FACET_CATEGORIA));

			return new Page(results, filter.getPage(), fullTextQuery.getResultSize());
		}
		return new Page();
	}

	private Query criarQuery(Filter filter, FullTextEntityManager fullTextEntityManager, QueryBuilder qb) {


		List<Query> queries = new ArrayList<Query>();


		if(filter.getOcupacao() != null) {
			queries.add(criarQueryComOcupacao(qb, filter.getOcupacao()));
		}


		if(filter.getTermo() != null && !filter.getTermo().isEmpty()){
			queries.add(criarQueryComTermo(filter, fullTextEntityManager, qb));
		}

		if(queries.isEmpty()) {
			return null;
		}else if(filter.getUf() != null) {
			queries.add(criarQueryComUF(qb, filter.getUf()));
		}

		if(queries.size() == 1) {
			return queries.get(0);
		}

		else {
			@SuppressWarnings("rawtypes")
			BooleanJunction<BooleanJunction> qj = qb.bool();
			for(Query q :queries) {
				qj.must(q);
			}
			return qj.createQuery();
		}
	}

	private Query criarQueryComOcupacao(QueryBuilder qb, Long ocupacao) {
		return qb.keyword()
				.onField(Anuncio.OCUPACAO_ID)
				.matching(ocupacao)
				.createQuery();
	}


	private Query criarQueryComUF(QueryBuilder qb, String uf) {
		return qb.keyword()
				.onField(Anuncio.UF)
				.matching(uf)
				.createQuery();
	}

	private FullTextQuery criarFullTextQuery(Filter filter, FullTextEntityManager fullTextEntityManager,
			Query query) {
		FullTextQuery fullTextQuery;
		fullTextQuery = fullTextEntityManager.createFullTextQuery(query, Anuncio.class);

		fullTextQuery.setProjection(Anuncio.ID);
		fullTextQuery.setFirstResult((filter.getPage() -1) * maxResult);
		fullTextQuery.setMaxResults(maxResult);

		return fullTextQuery;
	}

	@SuppressWarnings("unchecked")
	private List<AnuncioDTO> realizarConsulta(FullTextQuery q) {
		return anuncioRepository.getAnunciosById(getIds(q.getResultList()));
	}

	private Query criarQueryComTermo(Filter filter, FullTextEntityManager fullTextEntityManager, QueryBuilder qb) {
		Query query = qb
				.keyword()
				.onFields(Anuncio.TITULO, Anuncio.DESCRICAO, Anuncio.OCUPACAO_DESCRICAO)
				.matching(filter.getTermo())
				.createQuery();
		return query;
	}

	private Query criarQueryUltimosAnuncios(Filter filter,	FullTextEntityManager fullTextEntityManager, QueryBuilder qb) {
		
		if(filter.getUf() != null) {
			return criarQueryComUF(qb, filter.getUf());
		}
		
		Query query = qb
				.all()
				.createQuery();

		return query;
	}

	private List<Long> getIds(List<Object[]> ids) {
		List<Long> result = new ArrayList<>();

		for(int i = 0; i< ids.size(); i++){
			result.add((Long) ids.get(i)[0]);
		}
		return result;
	}

	private FacetingRequest setFacetCategoria( QueryBuilder qb){

		FacetingRequest facetingRequest = qb.facet()
				.name(FACET_CATEGORIA)
				.onField(Anuncio.OCUPACAO_DESCRICAO)
				.discrete()
				.orderedBy(FacetSortOrder.COUNT_DESC)
				.includeZeroCounts(false)
				.maxFacetCount(10)
				.createFacetingRequest();

		return facetingRequest;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AnuncioDTO> geoConsulta(Filter filter) {

		FullTextEntityManager fullTextEntityManager = getFullTextEntityManager(entityManager);

		QueryBuilder qb = fullTextEntityManager.getSearchFactory()
				.buildQueryBuilder().forEntity(Anuncio.class).get();

		Query query = null;
		FullTextQuery fullTextQuery = null;

		query = qb.bool()
				.must(criarQueryComTermo(filter, fullTextEntityManager, qb))
				.must(criarSpatialQuery(filter, fullTextEntityManager, qb))
				.createQuery();

		fullTextQuery =fullTextEntityManager.createFullTextQuery(query, Anuncio.class);

		fullTextQuery.setProjection("id");
		Sort sort = qb
				.sort()
				.byField(Anuncio.DATA_CADASTRO).desc() 
				.createSort();
		fullTextQuery.setSort(sort);

		if(fullTextQuery.getResultSize() > 0){
			return anuncioRepository.getAnuncioComDadosLocalizacao(getIds(fullTextQuery.getResultList()));

		}

		return null;
	}

	private Query criarSpatialQuery(Filter filter, FullTextEntityManager fullTextEntityManager, QueryBuilder qb) {
		Query query = qb
				.spatial()
				.within(filter.getRadius(), Unit.KM)
				.ofLatitude(filter.getLatitude())
				.andLongitude(filter.getLongitude())
				.createQuery();

		return query;
	}
}