package br.com.servant.negocio.seguranca;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class ServantHttp403ForbiddenEntryPoint implements AuthenticationEntryPoint {

	private final Logger log = LoggerFactory.getLogger(ServantHttp403ForbiddenEntryPoint.class);

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException arg2)
			throws IOException,
			ServletException {

		log.debug("Pre-authenticated entry point called. Rejecting access");
		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Access Denied");
	}
}