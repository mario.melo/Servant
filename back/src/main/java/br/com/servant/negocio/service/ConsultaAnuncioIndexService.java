package br.com.servant.negocio.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.search.jpa.FullTextEntityManager;

import br.com.servant.dtos.AnuncioDTO;
import br.com.servant.resource.controller.ConsultaController.Filter;
import br.com.servant.util.Page;

public interface ConsultaAnuncioIndexService {
	
	Page consultar(Filter filter);
	
	default FullTextEntityManager getFullTextEntityManager(EntityManager entityManager){
		return  org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);
	}

	List<AnuncioDTO> geoConsulta(Filter filter);
}
