INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(1, 'Para o Lar', null, null, 'icon-home');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(2, 'Saúde', null, null, 'icon-heart2');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(3, 'Beleza, Estética e Bem Estar', null, null, 'fa fa-bath');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(4, 'Construção / Reparo', null, null, 'fa fa-building');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(5, 'Veiculos', null, null, 'fa fa-car');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(6, 'Transporte / Mudanças', null, null, 'fa fa-bus');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(7, 'Vestiario', null, null, 'fa fa-female');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(8, 'Festas e Eventos', null, null, 'icon-forkandspoon');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(9, 'Educação', null, null, 'fa fa-graduation-cap');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(10, 'Tecnologia', null, null, 'fa fa-laptop');
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao, icone)VALUES(11, 'Outros Serviços', null, null, 'fa fa-briefcase');


-- Para O Lar

INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(12, 'Diarista', 1, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(13, 'Empregada Doméstica', 1, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(14, 'Babá', 1, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(15, 'Cuidadora', 1, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(16, 'Jardineiro', 1, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(17, 'Outros', 1, null);

 -- Saude
 
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(18, 'Médicos', 2, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(19, 'Enfermeiros', 2, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(20, 'Terapia Alternativa', 2, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(21, 'Educação Fisica', 2, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(22, 'Outros', 2, null);

-- Beleza, Estética e Bem Estar


INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(23, 'Manicure/Pedicure', 3, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(24, 'Cabeleireiro', 3, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(25, 'Depilação', 3, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(26, 'Outros', 3, null);

-- Construção / Reparo

INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(27, 'Empreiteiro', 4, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(28, 'Engenheiro', 4, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(29, 'Arquiteto', 4, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(30, 'Reparos', 4, null);
INSERT INTO servant.ocupacao(id, descricao, id_ocupacao_pai, ordem_exibicao)VALUES(31, 'Outros', 4, null);


